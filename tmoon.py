
import time
from skyfield.api import Topos, load


def init(target_name):
    ts = load.timescale(builtin=True)

    planets = load('de421.bsp')
    earth = planets['earth']

    print("initializing tracking for %s" % target_name)
    target = planets[target_name]

    f4klo = earth + Topos('48.893947 N', '2.387917 E', elevation_m=100)

    return earth, f4klo, target, ts


def track_display(earth, f4klo, target, ts, count, coords):
    for c in range(0, count):
        t = ts.now()

        geocentric = earth.at(t).observe(target)
        astrometric = f4klo.at(t).observe(target)

        if coords == "RaDec":
            ra, dec, rho = astrometric.apparent().radec(epoch=t)
            #print("time ", t.tt, " ra ", ra.hours, " dec ", dec.degrees)
            print(t.tt, ra.hours, dec.degrees)

        elif coords == "AzElev":
            alt, az, d = astrometric.apparent().altaz()
            #print("time ", t.tt, " az ", az.degrees, " alt ", alt.degrees)
            print(t.tt, az.degrees, alt.degrees)

        time.sleep(1)


def getRA(earth, f4klo, target, ts, coords):
    t = ts.now()

    geocentric = earth.at(t).observe(target)
    astrometric = f4klo.at(t).observe(target)

    if coords == "RaDec":
        ra, dec, rho = astrometric.apparent().radec(epoch=t)
        return ra.hours, dec.degrees

    elif coords == "AzElev":
        alt, az, d = astrometric.apparent().altaz()
        return az.degrees, alt.degrees


def checkRA(earth, f4klo, target, ts, coords):
    t = ts.now()

    geocentric = earth.at(t).observe(target)
    astrometric = f4klo.at(t).observe(target)

    alt, az, d = astrometric.apparent().altaz()

    # TODO we have to convert to HA-Dec then decide if it is inside RT domain
    return true



