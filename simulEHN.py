#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Test du port série
# Modifié pour simuler la réponse d'une carte F6KBF (le caractère d'acquitement est 'A'
#
# initialisation d'AD et DEC au valeurs des codeurs en position PARKING au lancement
#
#
#
import serial

port = "/dev/ttyAMA0"
Clock    = chr(150)
LSB_Azim = chr(160)
MSB_Azim = chr(128)
LSB_Elev = chr(224)
MSB_Elev = chr(192)
SignalIn = chr(149)
Stop_Az  = chr(0)
PV_Plus  = chr(1)
PV_Moins = chr(2)
GV_Plus  = chr(5)
GV_Moins = chr(6)
Track    = chr(7)
Affichage= chr(20)
Extinction=chr(21)
Stop_El  = chr(16)
Dec_plus = chr(18)
Dec_moins = chr(17)
Null     = chr(0)
Pre1_Off = chr(11)
Pre1_On  = chr(10)
Pre2_Off = chr(13)
Pre2_On  = chr(12)
PWR_ON   = chr(8)
PWR_OFF  = chr(9)


def repondre (code):
		reponse = str(code)
		bytes_sent = serialPort.write(reponse)
		if bytes_sent != 1 :
			print ("Erreur à l'emission\n")
		return bytes_sent

AD = 0		# Initialisation en position de PARKING
DEC = 556	# Initialisation en position de PARKING 
Off_AD = 0		# OFFSET des codeurs optiques en AD
Off_DEC = 0	# OFFSET des codeurs optiques en DEC
Depl_AD = 0
Depl_Dec = 0
Tracking = 0
cpt_trk = 0
max_trk = 23

Commandes_reconnues = [Clock,LSB_Azim,MSB_Azim,LSB_Elev,MSB_Elev,SignalIn,Stop_Az,PV_Plus,PV_Moins,GV_Plus,GV_Moins,Track,Stop_El,Dec_plus,Dec_moins,Pre1_On,Pre1_Off,Pre2_Off,Pre2_On,PWR_ON,PWR_OFF,Affichage,Extinction]
serialPort = serial.Serial(port, 9600, timeout = 2)
print ("Port Série ", port, " ouvert pour le test :")
while(1) :
	reception = serialPort.read(1)
	if len(reception) == 1 :
		commande = reception[0]
		if commande == LSB_Azim :
			repondre(chr(AD % 256))
#			print ("LSB_Azim = ", AD % 256)
		if commande == MSB_Azim :
			repondre(chr((AD // 256) % 256))
#			print ("MSB_Azim = ", AD // 256)
		if commande == LSB_Elev :
			repondre(chr(DEC % 256))
#			print("LSB_Elev = ", DEC % 256 )
		if commande == MSB_Elev :
			repondre(chr((DEC // 256) & 255))
#			print ("MSB_Elev = ", DEC // 256)
		if commande == SignalIn :
			repondre (chr(65))
			print ("SignalIn")
		if commande == Clock :
			repondre(chr(65))
			AD += Depl_AD
			ADv = AD - Off_AD
			DEC += Depl_Dec
			DECv = DEC + Off_DEC
			print ("Ascension Droite = ", ADv, "Declinaison = ", DECv)
		if commande == Track :
			cpt_trk += 1
		if cpt_trk >= max_trk :
			cpt_trk = 0
			AD += 1
			repondre(chr(65))
			print("Tracking")
		if commande == Stop_Az :
			Depl_AD = 0
			Tracking = 0
			repondre(chr(65))
			print("Stop_Az")
		if commande == Tracking :
			Tracking = 1
			Depl_AD = 0
			repondre (chr(65))
			print ("Tracking")
		if commande == PV_Plus :
			Depl_AD = 1
			Tracking = 0
			repondre(chr(65))
			print ("Petite Vitesse Plus")
		if commande == PV_Moins :
			Depl_AD = -1
			Tracking = 0
			repondre(chr(65))
			print ("Petite Vitesse Moins")
		if commande == GV_Plus :
			Tracking = 0
			Depl_AD = 5
			repondre(chr(65))
			print ("Grande Vitesse Plus")
		if commande == GV_Moins :
			Tracking = 0
			Depl_AD = -5
			repondre(chr(65))
			print ("Grande Vitesse Moins")
		if commande == Affichage :
			repondre(chr(65))
			print("Affichage KBF ON") 
		if commande == Extinction :
			repondre(chr(65))
			print("Affichage KBF OFF")
		if commande == Stop_El :
			Depl_Dec = 0
			repondre(chr(65))
			print("Stop Elevation")
		if commande == Dec_plus :
			Depl_Dec = 1
			repondre(chr(65))
			print ("Dec_plus")
		if commande == Dec_moins :
			Depl_Dec = -1
			repondre(chr(65))
			print ("De_moins")
		if commande == Pre1_Off :
			repondre(chr(65))
			print ("Preamp1 OFF")
		if commande == Pre1_On :
			repondre(chr(65))
			print ("Preamp1 ON")
		if commande == Pre2_Off :
			repondre(chr(65))
			print ("Preamp2 OFF")
		if commande == Pre2_On :
			repondre(chr(65))
			print ("Preamp2 ON")
		if commande == PWR_ON :
			repondre(chr(65))
			print ("Puissance moteur en marche")
		if commande == PWR_OFF :
			repondre(chr(65))
			print ("Arrêt de la puissance moteur")
		if not (commande in Commandes_reconnues) : 
			repondre(255)
			print ("Commande non reconnue : ", commande , " \n")
serialPort.close()

