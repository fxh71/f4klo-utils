#!/usr/bin/python3
"""Module for conversion of files between different coordinate systems
"""

import sys

from skyfield.api import Topos, load

import coords

action = sys.argv[1]
filename = sys.argv[2]
long = coords.deg2rad(float(sys.argv[3]))
lat = coords.deg2rad(float(sys.argv[4]))

if action == "azelev2hadec":
    coords.convert_file_azelev2hadec(filename, long, lat)
elif action == "hadec2azelev":
    coords.convert_file_hadec2azelev(filename, long, lat)
elif action == "radec2azelev":
    ts = load.timescale(builtin=True)
    coords.convert_file_radec2azelev(filename, long, lat, ts)

