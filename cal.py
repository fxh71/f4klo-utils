#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------
#logiciel de commande de monture
Version = 33
#
#
#
# Historique 
#---------------------------------------------------------
# rev.#    Date    # Commentaires
# 0.1 # 16/06/2018 # Période préhistorique.
# 0.2 # 17/01/2019 # paramétrage des déplacements.
# 0.3 # 17/01/2019 # mise en place des boucles de déplacements.
# 0.4 # 18/01/2019 # correction de bugs dans le paramétrage.
# 0.5 # 19/01/2019 # Commander la sortie du mode PARKED (si besoin) avant de lancer le premier déplacement
# 0.6 # 19/01/2019 # Commande d'alimentation des moteurs avant tout déplacement et Arrêt en fin de programme
# 0.7 # 20/01/2019 # Mise en marche du préampli 1 sur chaque position, et arrêt avant le déplacement suivant
# 0.8 # 20/08/2020 # Modification de la connexion au serveur pour prendre en compte le nouveau paramétrage.
# 0.9 # 29/09/2020 # Modification de la durée de pause (1mn) entre les différentes positions
# 0.10# 05/10/2020 # Synchronisation des déplacements sur la réception (voir ci-dessous)
# 0.11# 12/12/2020 # Prise en compte des refus de déplacement (déplacement en dehors du domaine d'évolution)
# 0.12# 19/12/2020 # sorties heure TUC fichier .txt
# 0.13# 20/12/2020 # sorties heure TUC au format ISO8601 avec fuseau horaire dans fichier -csv.txt et affiche les séquences d'observation / total
# 0.14# 20/12/2020 # Balayage en déclinaison de la grille d'observation de bas en haut et de haut en bas
                   # affiche le nom du serveur au démarrage
                   # les noms de fichiers sont étiquetés en fonction de l'heure de début
# 0.15# 22/12/2020 # suppression d'importations inutiles, selection du serveur grâce à la variable d'environnement F4KLO_SERVER
# 0.16# 23/12/2020 # Ajout de configuration de paramètres à la connexion Attention, cette version a besoin du driver V0.308 ou supérieur pour fonctionner
#                  # Le paramètre 6 (O ou Y) provoque le retour du radio télescope en parking. Pas de retour par défaut.  
# 0.17# 25/12/2020 # Positions au format fixe de "seulement" 5 décimales (...) et durée en secondes 4 décimales.
#                  # si status IDLE message "la position est en dehors du domaine visible" et exit()
# 0.18# 26/12/2020 # Inclusion de l'heure de la fin d'observation dans le fichier -csv. 
#                  # La durée d'observation en minutes est passée en 5ème paramètre (qui remplace le nombre de pas en déclinaison)
#                  # Le nombre de pas pour la déclinaison est TOUJOURS le même que celui pour l'AD. Matrice d'exploration Carré.
# 0.19# 27/12/2020 # Position affiche l'heure AD au format 00h00mn 
# 0.20# 29/12/2020 # Position affiche également la déclinaison au format 00°00'00"
#                  # toutes les heures sont TUC
#                  # Les paramètres des observations sont publiés toutes les minutes dans les fichiers journaux 
# 0.21#            # Version non publiée
# 0.22# 08/01/2020 # Entrée des heures au format 00:00:00 et déclinaisons 00:00:00 
# 0.23# 09/01/2020 # Lancer les déplacement à la 35eme seconde de l'acquisition.
# 0.24# 12/01/2021 # Ajout de commentaires pour le post-traitement
# 0.25# 28/01/2021 # Au lancement du programme, attente que le premier point se trouve dans le domaine d'évolution. 
# 0.26# 29/01/2021 # suppression d'une ligne vide dans le fichier de coordonnées. Ajout d'un paramètre pour nommer l'analyse
# 0.27# 07/02/2021 # Modification des print(f' 
# 0.28# 14/02/2021 # Mise en service du préampli 2 pendant toute l'analyse (pour mosaic.py)
# 0.29# 17/02/2021 # Autoriser un pas en DEC différent du pas en AD (ajout d'un paramètre)
# 0.30# 13/06/2022 # Introduction de la nouvelle fonction d'appel aux status telescope_radec.getState()==PyIndi.IPS_BUSY, PyIndi.IPS_ALERT et PyIndi.IPS_IDLE
# 0.31# 01/12/2022 # Les paramètres en ligne de commande n'étaient pas bien lus
#				   # Nom du serveur simulateur www1 remplacé par simulator
# 0.32# 06/12/2022 # Dans les boucles les coordonnées de départ étaient fausses :
# 0.33# 11/09/2023 # Menu explicatif possibilité entrer RA/DEC en sexadécimal - paramètre nombre de case impair : 3, 5, 7, 9, 11 pour possibilité de case centrale
#-------------------------------------------------------------------------------------
# Description :
# Synchronisation des déplacements sur la réception :
# Une fois arrivé sur la position demandé, le client va attendre la fin de la minute en cours
# A ce moment là, la réception va démarrer, l'heure et la position vont être enregistrées dans deux fichiers texte 
# pour retrouver la réception correspondante. 
# La position est maintenue pendant 60s (voir si 40s suffisent) avant de repartir sur la réception suivante.
#
#
import os
import PyIndi
import time
import sys
import threading
import datetime
import pytz

class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
    def newDevice(self, d):
        pass
    def newProperty(self, p):
        pass
    def removeProperty(self, p):
        pass
    def newBLOB(self, bp):
        global blobEvent
        print("new BLOB ", bp.name)
        blobEvent.set()
        pass
    def newSwitch(self, svp):
        pass
    def newNumber(self, nvp):
        pass
    def newText(self, tvp):
        pass
    def newLight(self, lvp):
        pass
    def newMessage(self, d, m):
        pass
    def serverConnected(self):
        pass
    def serverDisconnected(self, code):
        pass


def goto(ra,dec): 
    print("début du GOTO")
    # We want to set the ON_COORD_SET switch to engage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    print("telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
    telescope_on_coord_set[0].s=PyIndi.ISS_ON  # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not(telescope_radec):
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    print(" ")
    date_time_string = str(time.gmtime(time.time()).tm_hour) + ":" + str(time.gmtime(time.time()).tm_min) + ":" + str(time.gmtime(time.time()).tm_sec) + " TUC"
    print(date_time_string)
    print('Position de départ : AD = %.5f Décl.= %.5f ' % (telescope_radec[0].value,telescope_radec[1].value))
    print('Destination        : AD = %.5f Décl.= %.5f ' % (ra,dec))  
    telescope_radec[0].value = ra
    telescope_radec[1].value = dec
    indiclient.sendNewNumber(telescope_radec)
    time.sleep(1)
    # Attente de l'entrée dans le domaine
    while Initial and (telescope_radec.getState()==PyIndi.IPS_IDLE) :
        print ('attente de 30s avant nouvel essai')
        time.sleep(30)
        telescope_radec[0].value = ra
        telescope_radec[1].value = dec
        indiclient.sendNewNumber(telescope_radec)
        time.sleep(1)
    # Attente de la fin de déplacement
    while (telescope_radec.getState()==PyIndi.IPS_BUSY):
        print('Scope Moving %.5f %.5f ' %(telescope_radec[0].value,telescope_radec[1].value))
        time.sleep(2)
    if (telescope_radec.getState() == PyIndi.IPS_ALERT):
        print('erreur GOTO (Alert)')
        exit()
    if (telescope_radec.getState() == PyIndi.IPS_IDLE):
        print('IDLE : la destination est en dehors du domaine accessible !')
        exit()
    print('Tracking, attente début minute')
    now = time.gmtime(time.time()).tm_sec   
    while not (now == 0):
        print(now)
        time.sleep(1)
        now = time.gmtime(time.time()).tm_sec
    date_time_string = str(time.gmtime(time.time()).tm_hour) + ":" + str(time.gmtime(time.time()).tm_min) + ":" + str(time.gmtime(time.time()).tm_sec) + " TUC"
    print(date_time_string)
    
def conv_sexa(HMS):
    if HMS[0] == '-':
        HMS_pos = HMS[1:]
        negatif = True
    else :
        HMS_pos = HMS
        negatif = False
    ctrl = (len(HMS_pos) == 7) or (len(HMS_pos) == 8)
    if ctrl:
        if (len(HMS_pos) == 8) :
            ctrl = (HMS_pos[2] == ':') and (HMS_pos[5] == ':')
            heures = int(HMS_pos[0:2])
            minutes = int(HMS_pos[3:5])
            secondes = int(HMS_pos[6:8])
        else :
            ctrl = (HMS_pos[1] == ':') and (HMS_pos[4] == ':')
            heures = int(HMS_pos[0:1])
            minutes = int(HMS_pos[2:4])
            secondes = int(HMS_pos[5:7])
    if ctrl :
        if (negatif == False) :
            return (heures + (minutes / 60) + (secondes / 3600))
        else :
            return (-heures	- (minutes / 60) - (secondes / 3600))
    else :
        return (99.9)

# Application
print(" ")
print('Application : ', sys.argv[0])
# connect the serveur
indiclient = IndiClient()

server_env = os.getenv("F4KLO_SERVER", default=None)
if server_env != None and server_env == "CTRLDISH":
    # real gear
    print("connect to CTRLDISH")
    server="ctrldish.f4klo.ampr.org"
    indiclient.setServer(server,7625)
else:
    # simulator
    print("connect to SIMULATOR")
    server="simulator.f4klo.ampr.org"
    indiclient.setServer(server,7624)

print("server %s" % server)

# Heure de début
Debut = time.time()
print("Lancement", time.asctime(time.gmtime(time.time())), "TUC sur serveur ", server)

# Traitement des paramètres

# print(len(sys.argv), ' arguments : ',sys.argv)
if (len(sys.argv) < 3):
#   print ("Connexion à {indiclient.setServer}")
   print ("nombre de paramètres insufisants, il faut au moins 'Ascension Droite' et 'Déclinaison'.")
   print ("format : cal{Version}.py <Ascension Droite> <Déclinaison> [<pas AD>] [<pas DEC>] [<nb de pas en AD et Dec>] [<durée des observations>] [<parking Y ou O>] <nom de la cible>")
   print ("l'Ascension Droite est exprimée en heures décimales ou sexagésimales hh:mm:ss")
   print ("La Déclinaison est exprimée en degrés décimaux ou sexagésimaux dd:mm:ss")
   print ("La valeur du pas (en AD et en DEC est exprimée en degrés décimaux")  
   print ("Le nombre de pas est impair (3, 5, 7, 9, 11) pour qu'il existe une case centrale dans la matrice")
   print ("Utilise par défaut le simulateur (simulator.f4klo.ampr.org ")
   print ("pour utiliser le radiotélescope la ligne de commande est F4KLO_SERVER=CTRLDISH ./calxx.py RA DEC etc...  ")
   print ("Le 6ème paramètre est la durée des observations en minutes (1 par défaut)")
   print ("O ou Y en 7ème paramètre demande d'aller au parking en fin d'observation - C'est non par défaut)")
   print ("Nom de l'objet visé pour identification des fichiers")
   print ("Exemples de ligne de commande :    ")
   print ("F4KLO_SERVER=""./cal33.py 11:17:27 04:34:23 0.3 0.3 7 1 Y SOLEIL")
   print ("F4KLO_SERVER=CTRLDISH ./cal33.py 18:57:12 01:23:56 0.3 0.3 7 1 N W44")
   exit(2)
try:
   AD = float(sys.argv[1])
except:
   AD = conv_sexa(sys.argv[1])
   if AD > 24 :
	   print (AD,"impossible de convertir l'ascension droite : utiliser un format HH:MM:SS ou flottant")
	   exit(3)
try:
   Dec = float(sys.argv[2])
except:
   Dec = conv_sexa(sys.argv[2])
   if Dec > 90 :
	   print ("impossible de convertir la déclinaison : utiliser un format HH:MM:SS ou flottant")
	   exit(4)
if (len(sys.argv) >= 4):
   try:
      Pas = float(sys.argv[3])
   except:
      Pas = 1.0    # Pas de 1° pris par défaut
else:
   Pas = 1.0       # Pas de 1° pris en absence de paramètre
Pas_AD = Pas / 15.0
if (len(sys.argv) >= 5):
   try:
      Pas = float(sys.argv[4])
   except:
      Pas = 1.0    # Pas de 1° pris par défaut
else:
   Pas = 1.0       # Pas de 1° pris en absence de paramètre
if (len(sys.argv) >= 6):
   try: 
      Nb_AD = int(sys.argv[5])
   except:
      Nb_AD = 11 # 11 pas pris par défaut
else: 
   Nb_AD = 11    # 11 pas pris en absence d'argument

Nb_Dec = Nb_AD

if (len(sys.argv) >= 7):
   try:
      Duree_Observation = int(sys.argv[6])
   except:
      Duree_Observation = 1 # Durée d'observation par multiples d'une minute - Une par défaut
Retour_Parking = False
if (len(sys.argv) >= 8):
   Retour_Parking = (sys.argv[7] == 'Y' or sys.argv[7] == 'O' or sys.argv[7] == 'y' or sys.argv[7] == 'o')
nom_obs = ""      
if (len(sys.argv) >= 9):
	nom_obs = sys.argv[8]
	
# Résumé des paramètres choisis
#------------------------------
print("Paramètres de base")
print("------------------")
print("Nom de l'objet cible    = ", nom_obs)
print(" ")
print("Ascension Droite        = ", AD)
print("Déclinaison             = ", Dec)
print("Valeur de chaque pas    = ", Pas)
print("Nombre de pas en AD/Dec = ", Nb_AD)
print("Durée des observations  = ", Duree_Observation * 60, "secondes")
print("Retour en position de Parking : ",Retour_Parking)      
print(" ")
print("Paramètres calculés")
print("-------------------")
AD_min = AD - (Pas_AD * ((Nb_AD - 1) / 2)) 
AD_max = AD + (Pas_AD * ((Nb_AD - 1) / 2))
Dec_min = Dec - (Pas * ((Nb_Dec - 1) / 2))
Dec_max = Dec + (Pas * ((Nb_Dec - 1) / 2))
admin = str('{:.5F}'.format(AD_min))
print("Ascension Droite mini. :" , admin)
admax = str('{:.5F}'.format(AD_max))
print("Ascension Droite maxi. :" , admax)
decmin = str('{:.5F}'.format(Dec_min))
print("Déclinaison minimale   :" , decmin)
decmax = str('{:.5F}'.format(Dec_max))
print("Déclinaison maximale   :" , decmax)
print
 
if (not(indiclient.connectServer())):
     print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
     print("  indiserver indi_simulator_telescope indi_simulator_ccd")
     sys.exit(1)
time.sleep(1)           # Important : laisser du temps pour que la connexion s'établisse
 
dl=indiclient.getDevices()
for dev in dl:
    print(dev.getDeviceName())
    telescope = dev.getDeviceName()

# connect the scope
# telescope="Radiotelescope La Villette v0.1"
device_telescope=None
telescope_connect=None
 
# get the telescope device
device_telescope=indiclient.getDevice(telescope)
while not(device_telescope):
    time.sleep(0.5)
    device_telescope=indiclient.getDevice(telescope)
     
# wait CONNECTION property be defined for telescope
telescope_connect=device_telescope.getSwitch("CONNECTION")
time.sleep(0.5)
connection_mode=device_telescope.getSwitch("CONNECTION_MODE")
print("Télescope Connecté")
# if the telescope device is not connected, we do connect it
    # Property vectors are mapped to iterable Python objects
    # Hence we can access each element of the vector using Python indexing
    # each element of the "CONNECTION" vector is a ISwitch
connection_mode[0].s=PyIndi.ISS_ON  # the "SERIAL" switch
connection_mode[1].s=PyIndi.ISS_OFF # the "TCP" switch
indiclient.sendNewSwitch(connection_mode) # send this new value to the device
print("Modification du mode de connection terminée")

time.sleep(0.5)
auto_search = device_telescope.getSwitch("DEVICE_AUTO_SEARCH")
print("Test auto_search")
if (auto_search[0].s == PyIndi.ISS_OFF and auto_search[1].s == PyIndi.ISS_OFF) :
    auto_search[0].s=PyIndi.ISS_ON  # INDI_ENABLED = ON 
    auto_search[1].s=PyIndi.ISS_OFF # INDI_DISABLED = OFF
    indiclient.sendNewSwitch(auto_search) # send this new value to the device
    print("Modification de l'auto-search terminée")

time.sleep(0.5)
dome_policy = device_telescope.getSwitch("DOME_POLICY")
print("Test Dome_policy")
if (dome_policy[0].s == PyIndi.ISS_OFF and dome_policy[1].s == PyIndi.ISS_OFF) :
    dome_policy[0].s=PyIndi.ISS_ON  # DOME_IGNORED = ON 
    dome_policy[1].s=PyIndi.ISS_OFF # DOME_LOCKS = OFF
    indiclient.sendNewSwitch(dome_policy) # send this new value to the device
    print("Modification de Dome-Policy terminée")

# Activation (si besoin) de l'alimentation moteur
#------------------------------------------------
puissance = device_telescope.getSwitch("Puissance")
while not(puissance):
    time.sleep(0.5)
    puissance = device_telescope.getSwitch("Puissance")
LaisserALArret = False
print("Test de l'alimentation moteurs")
if (puissance[0].s == PyIndi.ISS_ON):
    print("Alimentation des moteurs")
    puissance[0].s = PyIndi.ISS_OFF # Arrêt moteur sur OFF
    puissance[1].s = PyIndi.ISS_ON  # Marche moteur sur ON
    indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur
    LaisserALArret = True 

# Check for PARKED mode
#----------------------

telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
while not(telescope_park):
    time.sleep(0.5)
    telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
print("test Parked")
if (telescope_park[0].s == PyIndi.ISS_ON):
    print ("Télescope PARKED")
    telescope_park[0].s = PyIndi.ISS_OFF # the parked switch is set to OFF
    telescope_park[1].s = PyIndi.ISS_ON  # the unparked switch is set to ON
    indiclient.sendNewSwitch(telescope_park) # send this new value to the device
    print ('Télescope débloqué de la position de parking')
 
# Ouverture du fichier de coordonnées
# -----------------------------------

year = time.gmtime(time.time()).tm_year
month = time.gmtime(time.time()).tm_mon
mday = time.gmtime(time.time()).tm_mday
date = str(year)+ "-" + str(month).zfill(2) +"-" + str(mday).zfill(2)

thour = time.gmtime(time.time()).tm_hour
tmin = time.gmtime(time.time()).tm_min
tsec = time.gmtime(time.time()).tm_sec
otime = str(thour).zfill(2) + "-" + str(tmin).zfill(2) + "-" + str(tsec).zfill(2)
otime2 = str(thour).zfill(2) + ":" + str(tmin).zfill(2) + ":" + str(tsec).zfill(2)

sorties_csv = "Coordonnees." + date + "T" + otime + ".txt"
Fichier_Sorties = open(sorties_csv,'w')
sorties = "# fichier coordonnees version " + str(Version) + "\n"
Fichier_Sorties.write(sorties)
sorties = "# postobs: " + date + " " + otime2 + " plutoDP141935" + date + ".csv " + sorties_csv + " " + str(Nb_AD) + " " + str(Nb_Dec) + " " + str(Pas_AD * 15) + " " + str(Pas) + " --mean " + nom_obs + "\n"
Fichier_Sorties.write(sorties)
if not(nom_obs == ""):
	sorties = "# target " + nom_obs + " \n# center_position " + str("{:.5F}".format(AD)) + " " + str("{:.5F}".format(Dec)) + "\n"
	Fichier_Sorties.write(sorties)
else:
	sorties = "# center_position " + str("{:.5F}".format(AD)) + " " + str("{:.5F}".format(Dec)) + "\n"
	Fichier_Sorties.write(sorties)	
sorties = "# matrix radec " + str(Nb_AD) + " " + str(Nb_Dec) + "\n"
Fichier_Sorties.write(sorties)
sorties = "# pas = " + str(Pas_AD * 15) + " " + str(Pas) + "\n"
Fichier_Sorties.write(sorties)
sorties = "# Duree d'observation = " + str(Duree_Observation) + "mn \n"
Fichier_Sorties.write(sorties)
# Entête format .CSV #yyyy-mm-ddThh:mm:ss TZ ra dec avec TZ une valeur numérique (0 pour UTC).
sorties = "#Date  Heure TUC, TZ , RA , DEC\n"
Fichier_Sorties.write(sorties)

Dec_Start = Dec_max
Pas = Pas * -1
Initial = True
preampli_2 = device_telescope.getSwitch("Preampli2")
while not(preampli_2):
      time.sleep(0.5)
      preampli_2 = device_telescope.getSwitch("Preampli2")
print("Mise en service du préampli 2")
preampli_2[0].s = PyIndi.ISS_OFF # Arrêt préampli 1 sur OFF
preampli_2[1].s = PyIndi.ISS_ON  # Marche Préampli 1 sur ON
indiclient.sendNewSwitch(preampli_2) # Envoyer la nouvelle valeur au serveur

# Inversion du sens de balayage en déclinaison à chaque pas d'AD (mode serpent !) 
for i_ad in range(Nb_AD):
    AD_courant = AD_max - (Pas_AD * i_ad)
    if Dec_Start == Dec_max:
        Dec_Start = Dec_min
        Pas = Pas * -1
    else:
        Dec_Start = Dec_max
        Pas = Pas * -1
    for i_dec in range(Nb_Dec):
        print("Observation : RA " + str(i_ad + 1)+ "/"  + str(Nb_AD) + " DEC " + str(i_dec + 1) + "/" + str(Nb_Dec)) 
        Dec_courant = Dec_Start + (Pas * i_dec)
        goto(AD_courant,Dec_courant)
        Initial = False
        preampli_1 = device_telescope.getSwitch("Preampli1")
        while not(preampli_1):
           time.sleep(0.5)
           preampli_1 = device_telescope.getSwitch("Preampli1")
        print("Mise en service du préampli 1")
        preampli_1[0].s = PyIndi.ISS_OFF # Arrêt préampli 1 sur OFF
        preampli_1[1].s = PyIndi.ISS_ON  # Marche Préampli 1 sur ON
        indiclient.sendNewSwitch(preampli_1) # Envoyer la nouvelle valeur au serveur
# Boucle sur le nombre de minutes demandés
        temps_observation = Duree_Observation
        while (temps_observation):
            date_time = time.asctime(time.gmtime(time.time()))
#            sortie = date_time[:19]+" UTC "+date_time[20:]
#            sortie = sortie + " R.A. = " + str("{:.5F}".format(AD_courant)) + " DEC. = " + str("{:.5F}".format(Dec_courant)) + "\n"
#            Fichier_Sortie.write(sortie)
            utc_now = pytz.utc.localize(datetime.datetime.utcnow())
            sorties = str(utc_now.isoformat('T',"seconds"))
            sorties = sorties + ",0," + str("{:.5F}".format(AD_courant)) + "," + str("{:.5F}".format(Dec_courant)) + "\n"           
            Fichier_Sorties.write(sorties)
# Conversion HM:MM:SS et deg:mn:sec           
            minutes = (AD_courant - int(AD_courant)) * 60
            secondes = int((minutes - int(minutes)) * 60)
            heureAD =  str(int(AD_courant)) + ":" + str(int(minutes)) + ":" + str(int(secondes))
            minutesDEC = (Dec_courant - int(Dec_courant)) * 60
            secondesDEC = int((minutesDEC - int(minutesDEC)) * 60)
            heureAD =   str(int(AD_courant))  + ":" + str(int(minutes))    + ":" + str(int(secondes))
            heureDEC =  str(int(Dec_courant)) + "°" + str(int(minutesDEC)) + "'" + str(int(secondesDEC))+'"'
            print('Position : AD = ',heureAD,' Décl.= ',heureDEC) 
            if (temps_observation == 1) :
                print("Pause de 35 secondes pendant l'acquisition des données")
                time.sleep(35)
            else :
                print("Pause de 60 secondes pendant l'acquisition des données")
                time.sleep(60)
            temps_observation -= 1
# fin de l'observation
        preampli_1[0].s = PyIndi.ISS_ON # Arrêt préampli 1 sur ON
        preampli_1[1].s = PyIndi.ISS_OFF # Marche Préampli 1 sur OFF
        indiclient.sendNewSwitch(preampli_1) # Envoyer la nouvelle valeur au serveur
        print("Arrêt du préampli 1")

#Fichier_Sortie.write("\n")
#Fichier_Sorties.write("\n")

#Fichier_Sortie.close
Fichier_Sorties.close
preampli_2 = device_telescope.getSwitch("Preampli2")
while not(preampli_1):
      time.sleep(0.5)
      preampli_2 = device_telescope.getSwitch("Preampli2")
print("Arrêt du préampli 2")
preampli_2[0].s = PyIndi.ISS_ON    # Arrêt préampli 2 sur ON
preampli_2[1].s = PyIndi.ISS_OFF   # Marche Préampli 2 sur OFF
indiclient.sendNewSwitch(preampli_2) # Envoyer la nouvelle valeur au serveur

fin = time.time()
duree = str('{:.4F}'.format(fin - Debut))
print("Durée : ",duree,"s")

if Retour_Parking :
    print("Retour sur la position de Parking")
    telescope_park = device_telescope.getSwitch("TELESCOPE_PARK")
    telescope_park[0].s = PyIndi.ISS_ON          # PARK = ON
    telescope_park[1].s = PyIndi.ISS_OFF         # UNPARK = OFF
    indiclient.sendNewSwitch(telescope_park)
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not (telescope_radec.getState() == PyIndi.IPS_BUSY) :
        print('attente déplacement')
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while (telescope_radec.getState() == PyIndi.IPS_BUSY):
        print('Scope Moving %.5f %.5f ' %(telescope_radec[0].value,telescope_radec[1].value))
#        print(f'Scope Moving {telescope_radec[0].value:.5f} {telescope_radec[1].value:.5f}')
        time.sleep(2)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    if (telescope_radec.getState() == PyIndi.IPS_IDLE) :
	    print("Radiotélescope en position de parking")
    	
if (LaisserALArret):
    print("Coupure Alimentation des moteurs")
    puissance[0].s = PyIndi.ISS_ON # Arrêt moteur sur ON
    puissance[1].s = PyIndi.ISS_OFF  # Marche moteur sur OFF
    indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur

print("fin de déplacement : déconnexion");
# if (device_telescope.isConnected()):
       # Property vectors are mapped to iterable Python objects
       # Hence we can access each element of the vector using Python indexing
       # each element of the "CONNECTION" vector is a ISwitch
#       telescope_connect[0].s=PyIndi.ISS_OFF  # the "CONNECT" switch
#       telescope_connect[1].s=PyIndi.ISS_ON # the "DISCONNECT" switch
#       indiclient.sendNewSwitch(telescope_connect) # send this new value to the device
