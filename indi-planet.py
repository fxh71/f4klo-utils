#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#man
# usage: indi-planet.py [<hours>] [<step_delay>] [--planet <planet>]
# defaults values:
#  hours -> 24 [hr]
#  step_delay -> 900 [s]
#  planet -> moon
#  tested "planets": mars, moon, sun
#

import PyIndi

import os
import sys
import time
import threading

import tmoon

import argparse

argc = len(sys.argv)

class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
    def newDevice(self, d):
        pass
    def newProperty(self, p):
        pass
    def removeProperty(self, p):
        pass
    def newBLOB(self, bp):
        global blobEvent
        print("new BLOB ", bp.name)
        blobEvent.set()
        pass
    def newSwitch(self, svp):
        pass
    def newNumber(self, nvp):
        pass
    def newText(self, tvp):
        pass
    def newLight(self, lvp):
        pass
    def newMessage(self, d, m):
        pass
    def serverConnected(self):
        pass
    def serverDisconnected(self, code):
        pass


def goto(ra,dec): 
    print("début du GOTO")
    # We want to set the ON_COORD_SET switch to disengage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    print("telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
    telescope_on_coord_set[0].getState()=PyIndi.ISS_ON  # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not(telescope_radec):
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    print('Position de départ R.A.= %.3f Décl.= %.3f' % (telescope_radec[0].value, telescope_radec[1].value))
    print('Destination        R.A.= %.3f Décl.= %.3f' % (ra, dec))
    telescope_radec[0].value = ra
    telescope_radec[1].value = dec
    indiclient.sendNewNumber(telescope_radec)
    time.sleep(2)
    # Attente de la fin de déplacement
    while (telescope_radec.s==PyIndi.IPS_BUSY):
    #while (abs(telescope_radec[0].value - ra) > 0.1 or abs(telescope_radec[1].value - dec) > 0.1):
        print('Scope Moving %.3f %.3f' % (telescope_radec[0].value, telescope_radec[1].value))
        time.sleep(2)
    print('Tracking')

######################################################################################################
# configuration

placement_delay = 6
powerdown_delay = 10


parser = argparse.ArgumentParser(description='options to indi-planet.py')
parser.add_argument("hours", nargs='?', default=24., type=float, help='number of hoours to run')
parser.add_argument("stepdelay", nargs='?', default=900, help='number of seconds between steps')
parser.add_argument("--planet", dest="planet", default="moon", help='change solar system object to follow')

args = parser.parse_args()
print("Solar system object to follow " + args.planet)
print("Number of hours to run " + str(args.hours))
print("Step delay in seconds " + str(args.stepdelay))
print("--")

want_preamp1 = False

######################################################################################################
# program starting

indiclient = IndiClient()

server_env = os.getenv("F4KLO_SERVER", default=None)
if server_env != None and server_env == "CTRLDISH":
    # real gear
    print("connect to CTRLDISH")
    server="ctrldish.f4klo.ampr.org"
    telescope="Radiotelescope La Villette";
    indiclient.setServer(server,7625)
else:
    # simulator
    print("connect to SIMULATOR")
    server="www1.f4klo.ampr.org"
    telescope="Simulateur Radiotelescope";
    indiclient.setServer(server,7624)

print("server %s" % server)

# get step number
hours = float(args.hours)

step_delay = float(args.stepdelay)
count = int(hours*3600/step_delay)

print("duration")
print("  hours ", hours)
print("  minutes ", 60*hours)
print("step_delay ", step_delay, "s")
print("count_number ", count)

# Traitement des paramètres
 
if (not(indiclient.connectServer())):
     print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
     print("  indiserver indi_simulator_telescope indi_simulator_ccd")
     sys.exit(1)
time.sleep(1)           # Important : laisser du temps pour que la connection s'établisse
 
# now telescope is fixed above
#dl=indiclient.getDevices()
#for dev in dl:
#    print(dev.getDeviceName())
#    telescope = dev.getDeviceName()

# connect the scope

device_telescope=None
telescope_connect=None
 
# get the telescope device
device_telescope=indiclient.getDevice(telescope)
while not(device_telescope):
    time.sleep(0.5)
    device_telescope=indiclient.getDevice(telescope)


# wait CONNECTION property be defined for telescope
telescope_connect=device_telescope.getSwitch("CONNECTION")
time.sleep(0.5)
connection_mode=device_telescope.getSwitch("CONNECTION_MODE")
print("Télescope Connecté")
# if the telescope device is not connected, we do connect it
    # Property vectors are mapped to iterable Python objects
    # Hence we can access each element of the vector using Python indexing
    # each element of the "CONNECTION" vector is a ISwitch
connection_mode[0].s=PyIndi.ISS_ON  # the "SERIAL" switch
connection_mode[1].s=PyIndi.ISS_OFF # the "TCP" switch
indiclient.sendNewSwitch(connection_mode) # send this new value to the device
print("Modification du mode de connection terminée")


# Activation (si besoin) de l'alimentation moteur
#------------------------------------------------
puissance = device_telescope.getSwitch("Puissance")
while not(puissance):
    time.sleep(0.5)
    puissance = device_telescope.getSwitch("Puissance")
LaisserALArret = False
print("Test de l'alimentation moteurs")
if (puissance[0].s == PyIndi.ISS_ON):
    print("Alimentation des moteurs")
    puissance[0].s = PyIndi.ISS_OFF     # Arrêt moteur sur OFF
    puissance[1].s = PyIndi.ISS_ON      # Marche moteur sur ON
    indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur
    LaisserALArret = True 


# Check for PARKED mode
#----------------------

telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
while not(telescope_park):
    time.sleep(0.5)
    telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
print("test Parked")
if (telescope_park[0].s == PyIndi.ISS_ON):
    print ("Télescope PARKED")
    telescope_park[0].s = PyIndi.ISS_OFF # the parked switch is set to OFF
    telescope_park[1].s = PyIndi.ISS_ON  # the unparked switch is set to ON
    indiclient.sendNewSwitch(telescope_park) # send this new value to the device
    print ('Télescope débloqué de la position de parking')

earth, f4klo, moon, ts = tmoon.init(args.planet)

# inserer ici le code pour le balayage
#-------------------------------------

ra, dec = tmoon.getRA(earth, f4klo, moon, ts, "RaDec")
#time.sleep(placement_delay)

if want_preamp1:
    print("Mise en service du préampli 1")
    preampli_1 = device_telescope.getSwitch("Preampli1")
    preampli_1[0].s = PyIndi.ISS_OFF         # Arrêt préampli 1 sur OFF
    preampli_1[1].s = PyIndi.ISS_ON          # Marche Préampli 1 sur ON
    indiclient.sendNewSwitch(preampli_1)     # Envoyer la nouvelle valeur au serveur
    print('Position : AD = %.3f Décl.= %.3f ' % (ra, dec)) 

# loop on positions
for i in range(0, count):
    ra, dec = tmoon.getRA(earth, f4klo, moon, ts, "RaDec")
    print('New Position AD = %.3f Décl.= %.3f ' % (ra, dec)) 
    goto(ra, dec)
    time.sleep(step_delay)

time.sleep(powerdown_delay)

# power down what necessary
if want_preamp1:
    preampli_1[0].s = PyIndi.ISS_ON          # Arrêt préampli 1 sur ON
    preampli_1[1].s = PyIndi.ISS_OFF         # Marche Préampli 1 sur OFF
    indiclient.sendNewSwitch(preampli_1)     # Envoyer la nouvelle valeur au serveur
    print("Arrêt du préampli 1")

# if (LaisserALArret):
print("Coupure Alimentation des moteurs")
puissance[0].s = PyIndi.ISS_ON # Arrêt moteur sur ON
puissance[1].s = PyIndi.ISS_OFF  # Marche moteur sur OFF
indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur

print("fin de déplacement : déconnexion");

#  WARNING: do not uncomment, unconnect ALL clients
# if (device_telescope.isConnected()):
       # Property vectors are mapped to iterable Python objects
       # Hence we can access each element of the vector using Python indexing
       # each element of the "CONNECTION" vector is a ISwitch
#       telescope_connect[0].s=PyIndi.ISS_OFF  # the "CONNECT" switch
#       telescope_connect[1].s=PyIndi.ISS_ON # the "DISCONNECT" switch
#       indiclient.sendNewSwitch(telescope_connect) # send this new value to the device

