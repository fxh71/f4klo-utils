#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------------------------------------------------------
Copyright (c) 2020, Bernard Pidoux (F6BVP - AI7BG), Patrick Dupré (F1EBK - W6NE), François-Xavier HUGOT (N5FXH) 
This software may be freely used, distributed, or modified, providing this header is not removed.

All rights reserved.

Permission to use, copy, modify, and distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
Except as contained in this notice, the name of a copyright holder shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization of the copyright holder.
-----------------------------------------------------------------------------------------------------------------

"""
#man
# usage: indi-planet.py [<hours>] [<step_delay>] [--planet <planet>]
# defaults values:
#  hours -> 24 [hr]
#  step_delay -> 900 [secondes]
#  planet -> moon
#  tested "planets": mars, moon, sun
#
# Version du 13-juin-2022  = planet130601 d'après indi-planet modification  fonction getState (telescope_radec.getState()
# Version du 27-juin-2022  = planet130602 test conditionnels si pas en simulateur alimentation moteurs et parked
#
# Ligne 92 : utilisation de la fonction getState (telescope_radec.getState()==PyIndi.IPS_BUSY): attente position cible
# Ligne 95 : utilisation de la fonction getState (telescope_radec.getState()==PyIndi.IPS_ALERT): sortie en cas d'erreur pilote 
# Ligne 99 : utilisation de la fonction getState (telescope_radec.getState()==PyIndi.IPS_IDLE): sortie du domaine observable 
#
# A FAIRE : 	Gérer les planètes gazeuses en ajoutant _barycenter accolé au nom. Ex : --planet jupiter_barycenter
#				Ajouter information de position toutes les 30 s pendant le tracking dans la boucle # loop on positions
#
# Attention : Nouvelle numérotation des versions
#-----#------------#----------------------------------------------------
# rev.#    Date    # Commentaires
# 0.1 # 11/06/2022 # Période préhistorique. Voir note ci-dessous
# 0.2 # 13/06/2022 # Utilisation de la méthode getState pour récupérer l'état d'un paramètre-INDI
# 0.3 # 27/06/2022 # Suppression des tests alimentation et mode Parked si on est sur le simulateur
# 0.4 # 27/06/2022 # Amélioration de la gestion de l'état du paramètre telescope_radec (voir note 2 ci-dessous).
# 0.5 # 10/07/2022 # Remise en service du test du mode Parked même si on est sur le simulateur.
# 0.6 # 10/07/2022 # Interruption du ralliement ou fin de la poursuite par appui sur la touche Entrée détectée par  kbhit().
# 0.7 # 11/07/2022 # Poursuite de la cible après le ralliement et Interruption ou fin de la poursuite par appui sur la touche Entrée détectée par  kbhit().
# 0.8 # 17/07/2022 # modification du goto
# 0.9 # 17/07/2022 # Télescope renvoyé au parking en fin d'observation (d'après cal30.py)
# 010# 24/07/2022 # définition de "simulator=False" dans le cas du radioélescope CTRLDISH
# 011# 24/07/2022 # Le suivi de la commande de retour au Parking était incorrect. Si interruption du ralliement ne pas afficher 'Destination atteinte'
# 012# 01/08/2022 # Coupure des moteurs inconditionnelle effective après retour au parking
# 013# 09/09/2022 # Affichage deux décimales pendant retour au parking
# 014# 29/09/2023 # www1 remplacé par nouveau nom simulator - usage ajouté - affichage plus précis Destination cible R.A.= %.6f Décl.= %.6f'
# 015# 30/09/2023 # affiche au départ les éphémérides de la cible
# 016# 11/11/2023 # affiche au départ le mode d'emploi de l'environnement simulator / ctrldish
# 017# 25/11/2023 # affiche les déplacements en heures minutes secondes
# 018# 09/01/2024 # corrige numéro de version et nom de l'application dans le mode d'emploi
# 019# 14/10/2024 # corrige la boucle d'attente pour avoir une nouvelle demande de GoTo seulement toutes les 15 minutes et une durée d'observation totale déterminée par "count"
# 020# 18/10/2024 # ajoute l'affichage de l'heure à chaque nouvelle boucle d'attente

# NOTE 1 
#-----------------------------------------------------------------------
# Ce programme est basé sur indi-planet de N5FXH qui constitue le période pré-historique
#
# NOTE 2 
#-----------------------------------------------------------------------
# Le paramètre telescope_radec peut prendre 3 états :
# IPS_OK : La monture est dans la bonne position et vise l'objet à étudier.
# IPS_BUSY : La monture est en train de se déplacer vers l'objet à étudier.
# IPS_IDLE : La dernière commande de déplacement a été rejetée car la destination se trouve en dehors du domaine de déplacement.
# 
# IPS_ALERT : ???

import PyIndi

import os
import sys
import time
import datetime
from time import gmtime, strftime
import threading
from select import select	# pour kbhit()

import tmoon

import argparse

VERSION = "(planet19.py) 14-octobre-2024"

argc = len(sys.argv)

class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
    def newDevice(self, d):
        pass
    def newProperty(self, p):
        pass
    def removeProperty(self, p):
        pass
    def newBLOB(self, bp):
        global blobEvent
        print("new BLOB ", bp.name)
        blobEvent.set()
        pass
    def newSwitch(self, svp):
        pass
    def newNumber(self, nvp):
        pass
    def newText(self, tvp):
        pass
    def newLight(self, lvp):
        pass
    def newMessage(self, d, m):
        pass
    def serverConnected(self):
        pass
    def serverDisconnected(self, code):
        pass

# fonctions de conversion en chaînes de caractères des degrés décimaux
# le paramètre entrée est en degrés décimaux - le paramètre de sortie est en degrés minutes secondes
def degre_chaine(deg):
	d = int(deg)
	m = int((deg - d) * 60)
	s = abs(int((deg - d - (m/60)) * 3600))
	degres =  str(d) +"° " + str(abs(m)) +"' " +str(s) +'"'
	return(degres)
# le paramètre entrée est en degrés décimaux - le paramètre en sortie est en heure minutes secondes
def degre_heure(deg):
	d = int(deg)
	m = int((deg - d) * 60)
	s = abs(int((deg - d - (m/60)) * 3600))
	heures =  str(d) +" H " + str(abs(m)) +" min " +str(s) +' sec'
	return(heures)

def kbhit():
    dr,dw,de = select([sys.stdin], [], [], 0)
    return dr != []

def goto(ra,dec): 
#    print("début du GOTO")
    # We want to set the ON_COORD_SET switch to disengage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
#    print("telescope_on_coord_set")
    # 
    # AUTORISER LA POURSUITE 
    telescope_on_coord_set[0].s=PyIndi.ISS_ON   # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not(telescope_radec):
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    print('Position de départ R.A. = %.6f Décl. = %.6f' % (telescope_radec[0].value, telescope_radec[1].value))
    telescope_radec[0].value = ra
    telescope_radec[1].value = dec
    print('Destination cible R.A. = %.6f Décl. = %.6f'  % (ra, dec))
    print('                  R.A. = ' + degre_heure(ra) + ' Décl. = '+ degre_chaine(dec))

# GO TO CIBLE RA DEC       
    indiclient.sendNewNumber(telescope_radec)
    time.sleep(1.5)

    if (telescope_radec.getState() == PyIndi.IPS_IDLE):
        print('IDLE : la destination est en dehors du domaine accessible !')
#            deconnecter(device_telescope)
        sys.exit(1)

# Attente de la fin de déplacement
    while (telescope_radec.getState()==PyIndi.IPS_BUSY):
        time.sleep(1.0)
        if (telescope_radec.getState() == PyIndi.IPS_ALERT):
            print('erreur GOTO (Alert)')
            print('IDLE : la destination est en dehors du domaine accessible !')
#          deconnecter(device_telescope)
            sys.exit(1)
        print('R.A. %.2f  Décl. %.2f ' % (telescope_radec[0].value,telescope_radec[1].value))
        print('R.A. ' + degre_heure(ra) + ' Décl. '+ degre_chaine(dec))
#  Interruption du ralliement par touche Entrée    
        ret = kbhit()
        if(ret == True):
            print(' ')
            print('Interruption du ralliement !')
# ICI ajouter commande d'arrêt du télescope en cas d'interruption manuelle !            
            break
    if not(ret):
        print('Destination atteinte !')
# fin du Goto
    return

######################################################################################################
# configuration

placement_delay = 6
powerdown_delay = 10


parser = argparse.ArgumentParser(description='options to indi-planet.py')
parser.add_argument("hours", nargs='?', default=24., type=float, help='number of hoours to run')
parser.add_argument("stepdelay", nargs='?', default=900, help='number of secondes between steps')
parser.add_argument("--planet", dest="planet", default="moon", help='change solar system object to follow')

args = parser.parse_args()
print("planet usage : ./planet <number of hours to run> <number of seconds between steps> < --planet name [default moon]") 
print("Solar system object to follow : " + args.planet)
print("Number of hours to run : " + str(args.hours))
print("Step delay in seconds : " + str(args.stepdelay))
print("--")

want_preamp1 = False

######################################################################################################
# program starting
print("Suivi des planètes avec le radiotélescope de la Villette version " + VERSION)
print("Par défaut l'application suivra le simulateur")
print("Pour le radiotélescope entrer la commande : F4KLO_SERVER=CTRLDISH ./planetXX.py")
print("--")
indiclient = IndiClient()
#Publie la date
current_time = strftime("%H:%M:%S", gmtime()) + "  TUC - " + datetime.datetime.now().strftime('%H:%M:%S') + "  Loc" 
print(current_time)

server_env = os.getenv("F4KLO_SERVER", default=None)
if server_env != None and server_env == "CTRLDISH":
    # real gear
    print("connect to CTRLDISH")
    server="ctrldish.f4klo.ampr.org"
    telescope="Radiotelescope La Villette";
    indiclient.setServer(server,7625)
    simulator = False

else:
    # simulator
    simulator = True
    print("connect to SIMULATOR")
    server="simulator.f4klo.ampr.org"
    telescope="Simulateur Radiotelescope";
    indiclient.setServer(server,7624)

print("server %s" % server)

# get step number
hours = float(args.hours)
step_delay = int(args.stepdelay)
count = int(hours*3600/step_delay)

print("duration")
print("  hours ", hours)
print("  minutes ", 60*hours)
print("step_delay ", step_delay, "secondes")
print("count_number ", count)

# Traitement des paramètres
 
if (not(indiclient.connectServer())):
     print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
     print("  indiserver indi_simulator_telescope indi_simulator_ccd")
     sys.exit(1)
time.sleep(1)           # Important : laisser du temps pour que la connection s'établisse
 
# now telescope is fixed above
#dl=indiclient.getDevices()
#for dev in dl:
#    print(dev.getDeviceName())
#    telescope = dev.getDeviceName()

# connect the scope

device_telescope=None
telescope_connect=None
 
# get the telescope device
device_telescope=indiclient.getDevice(telescope)
while not(device_telescope):
    time.sleep(0.5)
    device_telescope=indiclient.getDevice(telescope)


# wait CONNECTION property be defined for telescope
telescope_connect=device_telescope.getSwitch("CONNECTION")
time.sleep(0.5)
connection_mode=device_telescope.getSwitch("CONNECTION_MODE")
print("Télescope Connecté")
# if the telescope device is not connected, we do connect it
    # Property vectors are mapped to iterable Python objects
    # Hence we can access each element of the vector using Python indexing
    # each element of the "CONNECTION" vector is a ISwitch
connection_mode[0].s=PyIndi.ISS_ON  # the "SERIAL" switch
connection_mode[1].s=PyIndi.ISS_OFF # the "TCP" switch
indiclient.sendNewSwitch(connection_mode) # send this new value to the device
print("Modification du mode de connection terminée")

# Activation (si besoin) de l'alimentation moteur
#------------------------------------------------
if not simulator:
    puissance = device_telescope.getSwitch("Puissance")
    print("Test de l'alimentation moteurs")
    while not(puissance):
        time.sleep(0.5)
        puissance = device_telescope.getSwitch("Puissance")
    LaisserALArret = False
    if (puissance[0].s == PyIndi.ISS_ON):
        print("Alimentation des moteurs ON")
        puissance[0].s = PyIndi.ISS_OFF     # Arrêt moteur sur OFF
        puissance[1].s = PyIndi.ISS_ON      # Marche moteur sur ON
        indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur
        LaisserALArret = True 

# Check for PARKED mode
#----------------------

telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
print("test Parked")
while not(telescope_park):
    time.sleep(0.5)
    telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
if (telescope_park[0].s == PyIndi.ISS_ON):
    print ("Télescope PARKED")
    telescope_park[0].s = PyIndi.ISS_OFF # the parked switch is set to OFF
    telescope_park[1].s = PyIndi.ISS_ON  # the unparked switch is set to ON
    indiclient.sendNewSwitch(telescope_park) # send this new value to the device
    print ('Télescope débloqué de la position de parking')

if want_preamp1:
    print("Mise en service du préampli 1")
    preampli_1 = device_telescope.getSwitch("Preampli1")
    preampli_1[0].s = PyIndi.ISS_OFF         # Arrêt préampli 1 sur OFF
    preampli_1[1].s = PyIndi.ISS_ON          # Marche Préampli 1 sur ON
    indiclient.sendNewSwitch(preampli_1)     # Envoyer la nouvelle valeur au serveur

print('Interruption de la poursuite par touche Entrée')

# We set the desired coordinates
telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
while not(telescope_radec):
    time.sleep(0.5)
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")

# Boucle principale

ret = False
while not(ret) :
     #Publie la date
     current_time = strftime("%H:%M:%S", gmtime()) + "  TUC - " + datetime.datetime.now().strftime('%H:%M:%S') + "  Loc" 
     print(current_time)
     # Initialisation des coordonnées de la cible astronomique
     earth, f4klo, moon, ts = tmoon.init(args.planet)
     ra, dec = tmoon.getRA(earth, f4klo, moon, ts, "RaDec")
     print()
     print('Ephémerides de la cible : R.A.= %.6f Décl.= %.6f' % (ra, dec))
     print('                           '  + degre_heure(ra) + '    ' + degre_chaine(dec))
     print()
     #  Ralliement   goto(ra, dec)
     goto(ra, dec)
     # nouvelle période de poursuite
     # délai xx secondes
     if not(ret):
         print('Poursuite')
#         for i in range(0, count):
         for i in range(0, step_delay):
             time.sleep(1)
             if (telescope_radec.getState() == PyIndi.IPS_IDLE):
                 print('IDLE : la destination est en dehors du domaine accessible !')
#        deconnecter(device_telescope)
                 break 
             if (telescope_radec.getState() == PyIndi.IPS_ALERT):
                 print('erreur GOTO (Alert)')
#        deconnecter(device_telescope)
                 break
# Interruption de la poursuite par touche Entrée
             ret = kbhit()
             if(ret == True):
                 print(' ')
                 print('fin de la poursuite !')
                 break
     count = count -1
     if (count == 0):
        ret = True
        print(' ')
        print('fin de la poursuite !')
        break

#time.sleep(powerdown_delay)

telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
while not(telescope_park):
    time.sleep(0.5)

print ("Télescope renvoyé au PARKING")
telescope_park[0].s = PyIndi.ISS_ON  # the parked switch is set to ON
telescope_park[1].s = PyIndi.ISS_OFF # the unparked switch is set to OFF
indiclient.sendNewSwitch(telescope_park) # send this new value to the device

while not (telescope_radec.getState() == PyIndi.IPS_BUSY) :
    print('Attente déplacement')
    time.sleep(0.5)

telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
while (telescope_radec.getState() == PyIndi.IPS_BUSY):
    print('Télescope en mouvement vers le parking %.2f %.2f ' %(telescope_radec[0].value,telescope_radec[1].value))
    time.sleep(0.4)
# telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
if (telescope_radec.getState() == PyIndi.IPS_IDLE) :
    print ('Télescope arrivé au parking')

# power down what necessary
if want_preamp1:
    preampli_1[0].s = PyIndi.ISS_ON          # Arrêt préampli 1 sur ON
    preampli_1[1].s = PyIndi.ISS_OFF         # Marche Préampli 1 sur OFF
    indiclient.sendNewSwitch(preampli_1)     # Envoyer la nouvelle valeur au serveur
    print("Arrêt du préampli 1")

if not simulator:
# if (LaisserALArret):
    print("Coupure Alimentation des moteurs")
    puissance[0].s = PyIndi.ISS_ON # Arrêt moteur sur ON
    puissance[1].s = PyIndi.ISS_OFF  # Marche moteur sur OFF
    indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur

print("Déconnexion");

#  WARNING: do not uncomment, unconnect ALL clients
# if (device_telescope.isConnected()):
       # Property vectors are mapped to iterable Python objects
       # Hence we can access each element of the vector using Python indexing
       # each element of the "CONNECTION" vector is a ISwitch
#       telescope_connect[0].s=PyIndi.ISS_OFF  # the "CONNECT" switch
#       telescope_connect[1].s=PyIndi.ISS_ON # the "DISCONNECT" switch
#       indiclient.sendNewSwitch(telescope_connect) # send this new value to the device

