#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------
#logiciel de commande de monture
# $date 2018-06-16 $revision: 0001 $
#

#
# Historique 
#---------------------------------------------------------
# rev.#    Date    # Commentaires
# 0.1 # 16/06/2018 # Période préhistorique.
# 0.2 # 17/01/2019 # paramétrage des déplacements.
# 0.3 # 17/01/2019 # mise en place des boucles de déplacements.
# 0.4 # 18/01/2019 # correction de bugs dans le paramétrage.
# 0.5 # 19/01/2019 # Commander la sortie du mode PARKED (si besoin) avant de lancer le premier déplacement
# 0.6 # 19/01/2019 # Commande d'alimentation des moteurs avant tout déplacement et Arrêt en fin de programme
# 0.7 # 20/01/2019 # Mise en marche du préampli 1 sur chaque position, et arrêt avant le déplacement suivant


# std python imports
import os
import sys
import time
import threading

# external imports
import PyIndi
import jdcal

# local imports
import coords
 

class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
    def newDevice(self, d):
        pass
    def newProperty(self, p):
        pass
    def removeProperty(self, p):
        pass
    def newBLOB(self, bp):
        global blobEvent
        print("new BLOB ", bp.name)
        blobEvent.set()
        pass
    def newSwitch(self, svp):
        pass
    def newNumber(self, nvp):
        pass
    def newText(self, tvp):
        pass
    def newLight(self, lvp):
        pass
    def newMessage(self, d, m):
        pass
    def serverConnected(self):
        pass
    def serverDisconnected(self, code):
        pass


def goto(ra,dec): 
    print("début du GOTO")
    # We want to set the ON_COORD_SET switch to engage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    print("telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
    telescope_on_coord_set[0].s=PyIndi.ISS_ON  # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not(telescope_radec):
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    print("Position de départ R.A.= %.3f Décl.= %.3f" % (telescope_radec[0].value, telescope_radec[1].value))
    print("Destination        R.A.= %.3f Décl.= %.3f" % (ra, dec))
    telescope_radec[0].value = ra
    telescope_radec[1].value = dec
    indiclient.sendNewNumber(telescope_radec)
    # Attente de la fin de déplacement
    while (telescope_radec.s==PyIndi.IPS_BUSY):
        #print(f'Scope Moving {telescope_radec[0].value:.3f} {telescope_radec[1].value:.3f}')
        time.sleep(2)
    print('Tracking')


def activate():
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    print("telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
    telescope_on_coord_set[0].s=PyIndi.ISS_ON  # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
    #telescope_on_coord_set[2].s=PyIndi.ISS_OFF # SYNC
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope=device_telescope.getNumber("HORIZONTAL_COORD")
    while not(telescope):
        time.sleep(0.5)
        telescope=device_telescope.getNumber("HORIZONTAL_COORD")
    telescope[0].value = 180
    telescope[1].value = 10
    indiclient.sendNewNumber(telescope)



def getPosition(jd, lng, lat):
    print("Recherche de la position")
    # switch="HORIZONTAL_COORD"
    switch="EQUATORIAL_EOD_COORD"
    # switch="EQUATORIAL_COORD"
    gcoords=device_telescope.getNumber(switch)
    while not(gcoords):
        print(".")
        time.sleep(0.5)
        gcoords=device_telescope.getSwitch(switch)
    ra = gcoords[0].value
    dec = gcoords[1].value
    print("Telescope_equatorial_coords_radec %3f %3f" % (ra, dec))
    ha, dec = coords.radec2hadec(lng, lat, jd, coords.hr2rad(ra), coords.deg2rad(dec))
    print("Telescope_equatorial_coords_hadec %3f %3f" % (coords.rad2hr(ha), coords.rad2deg(dec)))
    az, elev = coords.hadec2azelev(lng, lat, ha, dec)
    print("Telescope_horizontal_coords_azelev %3f %3f" % (coords.rad2deg(az), coords.rad2deg(elev)))


# connect the server
indiclient=IndiClient()

server_env = os.getenv("F4KLO_SERVER", default=None)
if server_env != None and server_env == "CTRLDISH":
    # real gear
    print("connect to ctrldish")
    server="ctrldish.f4klo.ampr.org"
    indiclient.setServer(server,7625)
else:
    # simulator
    print("connect to simulator")
    server="www1.f4klo.ampr.org"
    indiclient.setServer(server,7624)

print("server %s" % server)

# Heure de début
Local_Time = time.localtime()
print ("heure de lancement : ",Local_Time[2],"/", Local_Time[1],"/",
       Local_Time[0]," ", Local_Time[3],":", Local_Time[4],":", Local_Time[5])
Debut = time.time()

utc = time.gmtime()
#print(utc)
jd = float(sum(jdcal.gcal2jd(utc[0], utc[1], utc[2]))) + (utc[5] + 60. * (utc[4] + 60. * utc[3])) / 86400.
print("julian_date ", jd)
 

# Traitement des paramètres
# print(len(sys.argv), ' arguments : ',sys.argv)
if (not(indiclient.connectServer())):
     print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
     print("  indiserver indi_simulator_telescope indi_simulator_ccd")
     sys.exit(1)
time.sleep(1)           # Important : laisser du temps pour que la connection s'établisse
 

dl=indiclient.getDevices()
for dev in dl:
    print(dev.getDeviceName())
    telescope = dev.getDeviceName()


# connect the scope
# telescope="Radiotelescope La Villette v0.1"
device_telescope=None
telescope_connect=None
 

# get the telescope device
device_telescope=indiclient.getDevice(telescope)
while not(device_telescope):
    time.sleep(0.5)
    device_telescope=indiclient.getDevice(telescope)
     

# wait CONNECTION property be defined for telescope
telescope_connect=device_telescope.getSwitch("CONNECTION")
time.sleep(0.5)
connection_mode=device_telescope.getSwitch("CONNECTION_MODE")
print("Télescope Connecté")
# if the telescope device is not connected, we do connect it
    # Property vectors are mapped to iterable Python objects
    # Hence we can access each element of the vector using Python indexing
    # each element of the "CONNECTION" vector is a ISwitch
connection_mode[0].s=PyIndi.ISS_ON  # the "SERIAL" switch
connection_mode[1].s=PyIndi.ISS_OFF # the "TCP" switch
indiclient.sendNewSwitch(connection_mode) # send this new value to the device
print("Modification du mode de connection terminée")


# Activation (si besoin) de l'alimentation moteur
#------------------------------------------------
# puissance = device_telescope.getSwitch("Puissance")
# while not(puissance):
    # time.sleep(0.5)
    # puissance = device_telescope.getSwitch("Puissance")
# LaisserALArret = False
# print("Test de l'alimentation moteurs")
# if (puissance[0].s == PyIndi.ISS_ON):
    # print("Alimentation des moteurs")
    # puissance[0].s = PyIndi.ISS_OFF # Arrêt moteur sur OFF
    # puissance[1].s = PyIndi.ISS_ON  # Marche moteur sur ON
    # indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur
    # LaisserALArret = True 


# Check for PARKED mode
#----------------------
telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
while not(telescope_park):
    time.sleep(0.5)
    telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
print("Test Parked")
if (telescope_park[0].s == PyIndi.ISS_ON):
    print ("Télescope PARKED")
    telescope_park[0].s = PyIndi.ISS_OFF # the parked switch is set to OFF
    telescope_park[1].s = PyIndi.ISS_ON  # the unparked switch is set to ON
    indiclient.sendNewSwitch(telescope_park) # send this new value to the device
    print ('Télescope débloqué de la position de parking')
 
# if (LaisserALArret):
    # print("Coupure Alimentation des moteurs")
    # puissance[0].s = PyIndi.ISS_ON # Arrêt moteur sur ON
    # puissance[1].s = PyIndi.ISS_OFF  # Marche moteur sur OFF
    # indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur


# real action: get position
# RT position
lng = coords.hr2rad(2.387916)
lat = coords.deg2rad(48.893979)

#activate()
#goto(20,5)
getPosition(jd, lng, lat)


print("fin de déplacement : déconnexion");
# if (device_telescope.isConnected()):
       # Property vectors are mapped to iterable Python objects
       # Hence we can access each element of the vector using Python indexing
       # each element of the "CONNECTION" vector is a ISwitch
#       telescope_connect[0].s=PyIndi.ISS_OFF  # the "CONNECT" switch
#       telescope_connect[1].s=PyIndi.ISS_ON # the "DISCONNECT" switch
#       indiclient.sendNewSwitch(telescope_connect) # send this new value to the device
fin = time.time()
duree = fin - Debut
print("Durée : ",duree,"s")
