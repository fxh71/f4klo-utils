INSTALL = install -v

.PHONY  : dist

all     :

install : $(HOME)/scripts
	$(INSTALL) klo-* $(HOME)/scripts

$(HOME)/scripts	dist/bin :
	mkdir $@

pylint	:
	pylint3 --indent-string="  " --max-line-length=128 klo-acq.py

dist    : dist/bin
	install klo-* dist/bin
	tar zcvf f4klo-utils.tgz --transform 's/dist/f4klo-utils/g' dist/

check   :
	pep8 coords.py
	pep8 track-star.py
	pep8 track-satellite.py

