#!/usr/bin/python3
"""Module for real time satellite positionning
"""

import sys
import math
import datetime
import ephem

#manual
# usage: python3 track-satellite tlefile sat inctime count long lat

#requirements
#pip3 install ephem

satfile=sys.argv[1]
sat=sys.argv[2]

f = open(satfile, 'r')
status=0
for line in f:
  lsat=line[0:len(sat)]
  lsat=lsat.replace(' ', '_')
  if lsat==sat:
    status=1
  elif status==1:
    line1=line.rstrip("\n")
    status=2
  elif status==2:
    line2=line.rstrip("\n")
    break

print("# "+sat)
print("# "+line1)
print("# "+line2)

print("# reading tle of satellite "+sat)
tle = ephem.readtle(sat, line1, line2)

inctime=float(sys.argv[3])
count=int(sys.argv[4])


#station = ephem.city('Paris')

station = ephem.Observer()
station.lon = sys.argv[5]
station.lat = sys.argv[6]

print("# lon "+str(station.lon))
print("# lat "+str(station.lat))

station.elevation = 0
station.horizon = '0'

station.date = '2020/03/26 08:00:00'
#station.date = datetime.utcnow()
#station.date = ephem.now()

tle.compute(station)

jd = ephem.julian_date(station.date)
#print("# date %g\n" % station.date)

for i in range(0, count):
  # referentiel geocentrique
  #print("%-14.12g %14.12g %14.12g" %(jd, 12.*tle.g_ra/math.pi, 180.*tle.g_dec/math.pi))
  # referentiel topocentrique
  print("%-18.16g %14.12g %14.12g" %(jd, 12.*tle.ra/math.pi, 180.*tle.dec/math.pi))
  # coordonnees astrometriques
  #print("%-18.16g %14.12g %14.12g" %(jd, 12.*tle.a_ra/math.pi, 180.*tle.a_dec/math.pi))
  jd += inctime/86400.
  station.date += inctime/86400.
  tle.compute(station)

#rising_time = station.next_pass(tle)[0]
#print('Horizon = 0 rising:', rising_time)


