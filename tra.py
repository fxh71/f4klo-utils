#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------------------
#logiciel de commande de monture
# $date 2018-06-16 $revision: 0001 $
#

#
# Historique 
#---------------------------------------------------------
# rev.#    Date    # Commentaires
# 0.1 # 17/08/2020 # Période préhistorique. repris à partir de cal7.
# 0.2 # 21/08/2020 # modification de l'adresse appelée pour correspondre à l'évolution du réseau
# 0.3 # 04/09/2020 # modification de la méthode de poursuite pour tenir compte de l'indicateur Eq.Coordinates
# 0.4 # 24/12/2020 # Ajout du système de sélection de serveur commun 
# 0.5 # 24/12/2020 # Modification du processus de déplacement (Mode Slew)
# 0.6 # 05/10/2022 # remplacements des "print(f," utilisation de la méthode getState() pour relire la valeur d'un paramètre Indi
# 0.7 # 25/11/2022 # remplacements de www1.f4klo par simulator.f4klo. Avertissement variable d'environnement pour CTRLDISH
# 0.8 # 02/12/2022 #
# 0.9 # 13/12/2022 # Place le pilote du radiotélescope en mode TRACK (quitte le mode SLEW) à la coupure des moteurs
#                    pour contourner le bogue sur la déclinaison qui survient quand on demande PARKING en mode SLEW


import os
import PyIndi
import time
import sys
import threading
     
class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
    def newDevice(self, d):
        pass
    def newProperty(self, p):
        pass
    def removeProperty(self, p):
        pass
    def newBLOB(self, bp):
        global blobEvent
        print("new BLOB ", bp.name)
        blobEvent.set()
        pass
    def newSwitch(self, svp):
        pass
    def newNumber(self, nvp):
        pass
    def newText(self, tvp):
        pass
    def newLight(self, lvp):
        pass
    def newMessage(self, d, m):
        pass
    def serverConnected(self):
        pass
    def serverDisconnected(self, code):
        pass


def conv_sexa(HMS):
    if HMS[0] == '-':
        HMS_pos = HMS[1:]
        negatif = True
    else :
        HMS_pos = HMS
        negatif = False
    ctrl = (len(HMS_pos) == 7) or (len(HMS_pos) == 8)
    if ctrl:
        if (len(HMS_pos) == 8) :
            ctrl = (HMS_pos[2] == ':') and (HMS_pos[5] == ':')
            heures = int(HMS_pos[0:2])
            minutes = int(HMS_pos[3:5])
            secondes = int(HMS_pos[6:8])
        else :
            ctrl = (HMS_pos[1] == ':') and (HMS_pos[4] == ':')
            heures = int(HMS_pos[0:1])
            minutes = int(HMS_pos[2:4])
            secondes = int(HMS_pos[5:7])
    if ctrl :
        if (negatif == False) :
            return (heures + (minutes / 60) + (secondes / 3600))
        else :
            return (-heures	- (minutes / 60) - (secondes / 3600))
    else :
        return (99.9)


def goto(ra,dec): 
	print("début du GOTO")
    # We want to set the ON_COORD_SET switch to engage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
	telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
	while not(telescope_on_coord_set):
		time.sleep(0.5)
		telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
	print("telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
	telescope_on_coord_set[0].s=PyIndi.ISS_ON  # TRACK
	telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
	indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
	telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
	while not(telescope_radec):
		time.sleep(0.5)
		telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
	print('Position de départ : AD = %.5f Décl.= %.5f ' % (telescope_radec[0].value,telescope_radec[1].value))
	print('Destination        : AD = %.5f Décl.= %.5f ' % (ra,dec))  
#	print(f'Position de départ R.A.= {telescope_radec[0].value:.3f} Décl.= {telescope_radec[1].value:.3f}')
#	print(f'Destination        R.A.= {ra:.3f} Décl.= {dec:.3f}')
	telescope_radec[0].value = ra
	telescope_radec[1].value = dec
	indiclient.sendNewNumber(telescope_radec)
 # wait for IPS_BUSY state
 #------------------------
	i = 0
	telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
	while ((i < 5) and not(telescope_radec.getState() == PyIndi.IPS_BUSY)):
		print ("attente début de déplacement")
		time.sleep(0.2)
		telescope_radec = device_telescope.getNumber("EQUATORIAL_EOD_COORD")
		i += 1
	if telescope_radec.getState() == PyIndi.IPS_IDLE :
		print('Destination rejetée par le driver')
		exit()
	if telescope_radec.getState() == PyIndi.IPS_BUSY :
		telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
		while not(telescope_on_coord_set):
			time.sleep(0.5)
			telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
		telescope_on_coord_set[0].s=PyIndi.ISS_OFF  # TRACK
		telescope_on_coord_set[1].s=PyIndi.ISS_ON   # SLEW
		indiclient.sendNewSwitch(telescope_on_coord_set)
		print("Déplacement en mode SLEW")
	telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
	while not(telescope_radec):
		time.sleep(0.5)
		telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
   
    # Wait for the antenna to be in the position

	while (telescope_radec.getState() == PyIndi.IPS_BUSY):
		print('Scope Moving %.5f %.5f ' %(telescope_radec[0].value,telescope_radec[1].value))
#		print(f'Scope Moving {telescope_radec[0].value:.3f} {telescope_radec[1].value:.3f}')
		time.sleep(2)
		telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    # request to stop the tracking
	if telescope_radec.getState() == PyIndi.IPS_IDLE:
		print ("Télescope arrivé en position")
#
# Repasse en mode TRACK pour éviter le bogue sur l'AD quand demande de retour au Parking 
	telescope_on_coord_set[0].s=PyIndi.ISS_ON  # TRACK
	telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
	indiclient.sendNewSwitch(telescope_on_coord_set)
	print ("Télescope mode SLEW = OFF repasse en mode TRACK = ON")
             
# connect the server

indiclient=IndiClient()
server_env = os.getenv("F4KLO_SERVER", default=None)
if server_env != None and server_env == "CTRLDISH":
    # real gear
    print("connect to CTRLDISH")
    server="ctrldish.f4klo.ampr.org"
    indiclient.setServer(server,7625)
else:
    # simulator
    print("connect to SIMULATOR")
    server="simulator.f4klo.ampr.org"
    indiclient.setServer(server,7624)

print("server %s" % server)

print("Pour controler le radiotélescope, indiquer la variable d'environnement F4KLO_SERVER à CTRLDISH avec la commande :")
print("export F4KLO_SERVER CTRLDISH")
print("ou bien lancer F4KLO_SERVER=CTRLDISH ./tra9.py")
#otherwise, simulator will be used

# Heure de début

Local_Time = time.localtime()
print ("heure de lancement : ",Local_Time[2],"/",Local_Time[1],"/",Local_Time[0]," ",Local_Time[3],":",Local_Time[4],":",Local_Time[5])
Debut = time.time()
 
# Traitement des paramètres

# print(len(sys.argv), ' arguments : ',sys.argv)
if (len(sys.argv) < 3):
   print ("nombre de paramètres insufisants, il faut au moins 'Ascension Droite' , 'Déclinaison' et délai.")
   print ("format : tra8.py <Ascension Droite> <Déclinaison> <délai>")
   print ("l'Ascension Droite est exprimée en HH:MM:SS ou heures décimales")
   print ("La Déclinaison est exprimée en DD:MM:SS ou degrés décimaux")
   print ('le délai de positionnement avant AD objet est exprimé en minutes')
   exit(2)


try:
   AD = float(sys.argv[1])
except:
   AD = conv_sexa(sys.argv[1])
   if AD > 24 :
	   print (AD,"impossible de convertir l'ascension droite : utiliser un format HH:MM:SS ou flottant")
	   exit(3)
print(" ")
print("Ascension droite : ", sys.argv[1], "  ", AD)       
try:
   Dec = float(sys.argv[2])
except:
   Dec = conv_sexa(sys.argv[2])
   if Dec > 90 :
	   print ("impossible de convertir la déclinaison : utiliser un format HH:MM:SS ou flottant")
	   exit(4)
print("Déclinaison      : ", sys.argv[2], "  ", Dec)       
print(" ")
try:
	delai = int(sys.argv[3])
except:
	delai = 15
	print('Délai fixé à 15mn')
	
	
# Résumé des paramètres choisis
#------------------------------
print("Paramètres de base")
print("------------------")
print(" ")
print("Ascension Droite     = ", AD)
print("Déclinaison          = ", Dec)
print("Délai avant AD       = ", delai,"min")
print(" ")
print
 
if (not(indiclient.connectServer())):
     print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
     print("  indiserver indi_simulator_telescope indi_simulator_ccd")
     sys.exit(1)
time.sleep(1)           # Important : laisser du temps pour que la connection s'établisse
 
dl=indiclient.getDevices()
for dev in dl:
    print(dev.getDeviceName())
    telescope = dev.getDeviceName()

# connect the scope
# telescope="Radiotelescope La Villette v0.1"
device_telescope=None
telescope_connect=None
 
# get the telescope device
device_telescope=indiclient.getDevice(telescope)
while not(device_telescope):
    time.sleep(0.5)
    device_telescope=indiclient.getDevice(telescope)

# wait CONNECTION property be defined for telescope
telescope_connect=device_telescope.getSwitch("CONNECTION")
time.sleep(0.5)
connection_mode=device_telescope.getSwitch("CONNECTION_MODE")
print("Télescope Connecté")
# if the telescope device is not connected, we do connect it
    # Property vectors are mapped to iterable Python objects
    # Hence we can access each element of the vector using Python indexing
    # each element of the "CONNECTION" vector is a ISwitch
connection_mode[0].s=PyIndi.ISS_ON  # the "SERIAL" switch
connection_mode[1].s=PyIndi.ISS_OFF # the "TCP" switch
indiclient.sendNewSwitch(connection_mode) # send this new value to the device
print("Modification du mode de connection terminée")

# Power up (if necessary) the motors
#-----------------------------------
puissance = device_telescope.getSwitch("Puissance")
while not(puissance):
    time.sleep(0.5)
    puissance = device_telescope.getSwitch("Puissance")
LaisserALArret = False
print("Test de l'alimentation moteurs")
if (puissance[0].s == PyIndi.ISS_ON):
    print("Alimentation des moteurs")
    puissance[0].s = PyIndi.ISS_OFF     # Power Down motor to OFF
    puissance[1].s = PyIndi.ISS_ON      # Power up motor to ON
    indiclient.sendNewSwitch(puissance) # Send new value to the server
    LaisserALArret = True 

# Check for PARKED mode
#----------------------

telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
while not(telescope_park):
    time.sleep(0.5)
    telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
print("test Parked")
if (telescope_park[0].s == PyIndi.ISS_ON):
    print ("Télescope PARKED")
    telescope_park[0].s = PyIndi.ISS_OFF # the parked switch is set to OFF
    telescope_park[1].s = PyIndi.ISS_ON  # the unparked switch is set to ON
    indiclient.sendNewSwitch(telescope_park) # send this new value to the device
    print ('Télescope débloqué de la position de parking')

#Positionnement de la parabole vers un AD avancé du délai en minutes
#-------------------------------------------------------------------
AD -= (delai/60)
goto(AD,Dec)


# if (LaisserALArret):
print("Coupure Alimentation des moteurs")#
puissance[0].s = PyIndi.ISS_ON # Arrêt moteur sur ON
puissance[1].s = PyIndi.ISS_OFF  # Marche moteur sur OFF
indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur

print("fin de déplacement : déconnexion");
# if (device_telescope.isConnected()):
       # Property vectors are mapped to iterable Python objects
       # Hence we can access each element of the vector using Python indexing
       # each element of the "CONNECTION" vector is a ISwitch
#       telescope_connect[0].s=PyIndi.ISS_OFF  # the "CONNECT" switch
#       telescope_connect[1].s=PyIndi.ISS_ON # the "DISCONNECT" switch
#       indiclient.sendNewSwitch(telescope_connect) # send this new value to the device
fin = time.time()
duree = fin - Debut
print("Durée : ",duree,"s")
