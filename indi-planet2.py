
import os
import sys
import time
import threading

import PyIndi

import PyIndiKlo
import tmoon

######################################################################################################
#

# usage: python3 indi-planet2.py <count> <step>

######################################################################################################
# configuration

argc = len(sys.argv)

placement_delay = 6
powerdown_delay = 10

objname="moon"

if argc>2:
  step_delay = int(sys.argv[2])
else:
  step_delay = 900

want_preamp1 = False

# get step number

if len(sys.argv) == 1:
  count = 10    # 10 steps default
else:
  count = int(sys.argv[1])

######################################################################################################

earth, f4klo, object, ts = tmoon.init(objname)

ra1, dec1 = tmoon.getRA(earth, f4klo, object, ts, "RaDec")
#time.sleep(placement_delay)

# loop on positions
for i in range(1, count):
    ra0, dec0 = ra1, dec1
    ra1, dec1 = tmoon.getRA(earth, f4klo, object, ts, "RaDec")
    if coords.dstsphh(ra0, dec0, ra1, dec1)>1.:
        t0, t1 = t1, ts.now()
        print('New Position AD = %.3f Décl.= %.3f  DeltaT %20.20g' % (ra1, dec1, (t1-t0))) 
        goto(ra1, dec1)
    time.sleep(step_delay)

time.sleep(powerdown_delay)

PyIndiKlo.sendFullPowerDown()

