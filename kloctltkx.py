#!/usr/bin/python3

# Install tkinter before using

import os
import time
import datetime

import tkinter as tk

import skyfield.api

import coords

long = coords.deg2rad(2.387856)
lat = coords.deg2rad(48.893995)


def circlestatedemo(marcheflag, rastate, decstate):
  if not(marcheflag):
    marcheflag = True
  elif marcheflag and rastate == "Stop" and decstate == "Stop":
    rastate = "EstRapide"
  elif rastate == "EstRapide":
    rastate = "EstLent"
  elif rastate == "EstLent":
    rastate = "Poursuite"
  elif rastate == "Poursuite":
    rastate = "OuestLent"
  elif rastate == "OuestLent":
    rastate = "OuestRapide"
  elif rastate == "OuestRapide":
    rastate = "Stop"
    decstate = "Verti"
  elif decstate == "Verti":
    decstate = "Horizon"
  elif decstate == "Horizon":
    decstate = "Stop"
    marcheflag = False
  return marcheflag, rastate, decstate


def motion(event):
    x, y = event.x, event.y
    print('{}, {}'.format(x, y))


import PyIndiKlo

### define constants
sx = 1200
sy = 660

line1y = 350
line2y = 600

rx = 30
ry = 30
stopx = 600
stopy = line2y

declx = 800
decmx = 900
decrx = 1000

raestrapx = stopx-500
raestlenx = stopx-400
rastopx = stopx-300
rapoursx = stopx-200
raouestlenx = stopx-100
raouestrapx = stopx-0

font = "Terminal 10 bold"
font2 = "Terminal 20 bold"

ts = skyfield.api.load.timescale()

# modes are GTRACK and GSLEW
goto_mode = "GTRACK"
tracking = False

coord_asc="AD"


def setAscCoord(c):
  global coord_asc
  print("La nouvelle coordonnee est " + c)
  coord_asc = c


def setGotoMode(m):
  global goto_mode
  print("Apres un GOTO le mode sera " + m)
  goto_mode = m



parking = False

def inside(mx, my, px, py, rx, ry):
  # is click inside circle
  return ((px-mx)/rx)**2 + ((py-my)/ry)**2 < 1.



def callbackGoto(ra, dec, coord):
  global tracking, goto_mode
  print("callbackGoto " + coord + " " + ra.get() + " Dec " + dec.get())
  if ra.get() == "@" or dec.get() == "@":
    ra0, dec0 = PyIndiKlo.getRaDec()
  if ra.get() == '@':
    ra = ra0
  else:
    ra = float(ra.get())
  if dec.get() == "@":
    dec = dec0
  else:
    dec = float(dec.get())
  if coord == "AH":
    ra, _ = coords.hadec2radec(long, lat, ts.now(), coords.hr2rad(ra), coords.deg2rad(dec))
    ra = coords.rad2hr(ra)
  print("goto appele {} {}".format(ra, dec))
  print("  mode "+goto_mode)
  if goto_mode == "GTRACK":
    PyIndiKlo.gotoTrack(ra, dec)
    tracking = True
  elif goto_mode == "GSLEW":
    PyIndiKlo.gotoSlew(ra, dec)
  else:
    printf("invalid goto mode " + goto_mode)


def callbackParking(action):
  if action == "ON":
    print("MODE PARKING ACTIVE")
    PyIndiKlo.setSwitchOn("TELESCOPE_PARK", 0)
  elif action == "OFF":
    print("MODE PARKING DESACTIVE")
    PyIndiKlo.setSwitchOn("TELESCOPE_PARK", 1)
  else:
    print("&& commande invalide " + action)


def callbackPuissance(action):
  if action == "ON":
    print("PUISSANCE MOTEUR MISE A ON")
    PyIndiKlo.setSwitchOn("Puissance", 1)
  elif action == "OFF":
    print("PUISSANCE MOTEUR MISE A OFF")
    PyIndiKlo.setSwitchOn("Puissance", 0)
  else:
    print("&& commande invalide " + action)


def callbackAbort():
  print("Abort")
  PyIndiKlo.setSwitchOn("TELESCOPE_ABORT_MOTION", 0)


def init():
  global goto_mode, coord_var, oncoord_var
  # initialize all graphics stuff
  f = tk.Tk()
  f.title("CONTROLE F4KLO")

  c = tk.Canvas(f, width = sx, height = sy)
  c.pack()

  ### display all texts
  c.create_text(stopx,stopy-3*ry/2,fill="darkblue",font=font, text="ARRET D'URGENCE")

  arret = c.create_oval(stopx-rx, 200-ry, stopx+rx, 200+ry, fill = "darkred")
  c.create_text(stopx,200-3*ry/2,fill="darkblue",font=font, text="ARRET")

  c.create_text(declx,line1y-3*ry/2,fill="darkblue",font=font, text="DECLI-HORIZON")
  c.create_text(decrx,line1y-3*ry/2,fill="darkblue",font=font, text="DECLI-VERTI")

  c.create_text(raestrapx,line1y-3*ry/2,fill="darkblue",font=font, text="EST-RAPIDE")
  c.create_text(raestlenx,line1y-3*ry/2,fill="darkblue",font=font, text="EST-LENT")
  c.create_text(rapoursx,line1y-3*ry/2,fill="darkblue",font=font, text="POURSUITE")
  c.create_text(raouestlenx,line1y-3*ry/2,fill="darkblue",font=font, text="OUEST-LENT")
  c.create_text(raouestrapx,line1y-3*ry/2,fill="darkblue",font=font, text="OUEST-RAPIDE")

  ### display all lines
  declinem = c.create_line(decmx, line2y, decmx, line1y, fill="black", width=3)
  declineh1 = c.create_line(declx, line1y+50, declx, line1y+150, fill="black", width=3)
  declinev1 = c.create_line(decrx, line1y+50, decrx, line1y+150, fill="black", width=3)
  declineh2 = c.create_line(declx+100, line2y, declx, line1y+150, fill="black", width=3)
  declinev2 = c.create_line(decrx-100, line2y, decrx, line1y+150, fill="black", width=3)

  ralineer = c.create_line(raestrapx, line1y+50, raestrapx, line1y+150, fill="black", width=3)
  ralineel = c.create_line(raestlenx, line1y+50, raestlenx, line1y+150, fill="black", width=3)
  rastop = c.create_line(rastopx, line1y, rastopx, line1y+150, fill="black", width=3)
  ralinep = c.create_line(rapoursx, line1y+50, rapoursx, line1y+150, fill="black", width=3)
  ralineor = c.create_line(raouestrapx, line1y+50, raouestrapx, line1y+150, fill="black", width=3)
  ralineol = c.create_line(raouestlenx, line1y+50, raouestlenx, line1y+150, fill="black", width=3)

  ralineer = c.create_line(rapoursx, line2y, raestrapx, line1y+150, fill="black", width=3)
  ralineel = c.create_line(rapoursx, line2y, raestlenx, line1y+150, fill="black", width=3)
  rastop = c.create_line(rapoursx, line2y, rastopx, line1y+150, fill="black", width=3)
  ralinep = c.create_line(rapoursx, line2y, rapoursx, line1y+150, fill="black", width=3)
  ralineor = c.create_line(rapoursx, line2y, raouestrapx, line1y+150, fill="black", width=3)
  ralineol = c.create_line(rapoursx, line2y, raouestlenx, line1y+150, fill="black", width=3)

  ### SUR_COORDONNEES
  frame_oncoord = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_oncoord.pack(anchor="w")

  oncoord = tk.Label(frame_oncoord, text="SUR_COORDONNEES")
  oncoord.pack(side = tk.LEFT)

  oncoord_var = tk.StringVar()
  oncoord_var.set("GTRACK")
  oncoord_track = tk.Radiobutton(frame_oncoord, text="TRACK", variable=oncoord_var, value="GTRACK", command=(lambda:setGotoMode("GTRACK")))
  oncoord_track.pack(side=tk.LEFT)
  oncoord_slew = tk.Radiobutton(frame_oncoord, text="SLEW", variable=oncoord_var, value="GSLEW", command=(lambda:setGotoMode("GSLEW")))
  oncoord_slew.pack(side=tk.LEFT)

  ### ASCENSION_COORD BUTTON
  frame_coord = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_coord.pack(anchor="w")

  coord = tk.Label(frame_coord, text="COORDONNEES D'ASCENSION")
  coord.pack(side = tk.LEFT)

  coord_var = tk.StringVar()
  coord_var.set("AD")
  coord_ah = tk.Radiobutton(frame_coord, text="AD", variable=coord_var, value="AD", command=(lambda:setAscCoord("AD")))
  coord_ah.pack(side=tk.LEFT)
  coord_ad = tk.Radiobutton(frame_coord, text="AH", variable=coord_var, value="AH", command=(lambda:setAscCoord("AH")))
  coord_ad.pack(side=tk.LEFT)

  ### COORDONNEES BUTTON
  frame_poursuite = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_poursuite.pack(anchor="w")

  goto = tk.Label(frame_poursuite, text="COORDONNEES"); goto.pack(side = tk.LEFT)

  ratxt = tk.Label(frame_poursuite, text="AD/AH"); ratxt.pack(side = tk.LEFT)
  raentry = tk.Entry(frame_poursuite, text=""); raentry.pack(side = tk.LEFT)

  dectxt = tk.Label(frame_poursuite, text="Dec"); dectxt.pack(side = tk.LEFT)
  decentry = tk.Entry(frame_poursuite, text=""); decentry.pack(side = tk.LEFT)

  gotosend = tk.Button(frame_poursuite, text="ENVOI", command=(lambda: callbackGoto(raentry, decentry, coord_var.get())))
  gotosend.pack(side = tk.LEFT)

  gotoabort = tk.Button(frame_poursuite, text="ABORT", command=(lambda: callbackAbort()))
  gotoabort.pack(side = tk.LEFT)

  ### PUISSANCE MOTEURS BUTTON
  frame_puiss = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_puiss.pack(anchor="w")
  puiss = tk.Label(frame_puiss, text="PUISSANCE MOTEURS")
  puiss.pack(side = tk.LEFT)

  puiss_on = tk.Button(frame_puiss, text="MARCHE", command = (lambda: callbackPuissance("ON")))
  puiss_on.pack(side = tk.LEFT)
  puiss_off = tk.Button(frame_puiss, text="ARRET", command = (lambda: callbackPuissance("OFF")))
  puiss_off.pack(side = tk.LEFT)

  ### PARKING BUTTON
  frame_parking = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_parking.pack(anchor="w")
  park = tk.Label(frame_parking, text="PARKING")
  park.pack(side = tk.LEFT)

  parking_on = tk.Button(frame_parking, text="ACTIVE", command = (lambda: callbackParking("ON")))
  parking_on.pack(side = tk.LEFT)
  parking_off = tk.Button(frame_parking, text="DESACTIVE", command = (lambda: callbackParking("OFF")))
  parking_off.pack(side = tk.LEFT)

  return f, c


def callbackButton(event):
    global tracking
    print("you clicked at", event.x, event.y)
    com=None
    if inside(event.x, event.y, stopx, 200, rx, ry) or inside(event.x, event.y, stopx, line2y, rx, ry):
        com="ARRET"
        PyIndiKlo.setSwitchOff("TELESCOPE_MOTION_NS")
        PyIndiKlo.setSwitchOff("TELESCOPE_MOTION_WE")
        tracking = False
    if inside(event.x, event.y, rapoursx, line1y, rx, ry):
        com="POURSUITE"
        print("Controle "+com+ " not yet available")
    if inside(event.x, event.y, raouestlenx, line1y, rx, ry):
        com="OUEST-LENT"
        print("Controle "+com)
        PyIndiKlo.setSwitchOn("TELESCOPE_MOTION_WE", 0)
    if inside(event.x, event.y, raestlenx, line1y, rx, ry):
        com="EST-LENT"
        print("Controle "+com)
        PyIndiKlo.setSwitchOn("TELESCOPE_MOTION_WE", 1)
    if inside(event.x, event.y, decrx, line1y, rx, ry):
        com="VERTI"
        print("Controle "+com)
        PyIndiKlo.setSwitchOn("TELESCOPE_MOTION_NS", 0)
    if inside(event.x, event.y, declx, line1y, rx, ry):
        com="HORIZON"
        print("Controle "+com)
        PyIndiKlo.setSwitchOn("TELESCOPE_MOTION_NS", 1)
    if com != None:
        print("demande envoyee " + com)
    else:
        print("aucune demande envoyee")
    return com


marcheflag = False
rastate = "Stop"
decstate = "Stop"

f, c = init()

tm = time.time()
tml = tm


def update_control():
    global marcheflag, rastate, decstate, tm, tml, tracking, goto_mode
    #marcheflag, rastate, decstate = circlestatedemo(marcheflag, rastate, decstate)
    marcheflag, rastate, decstate = PyIndiKlo.getState(marcheflag, rastate, decstate)
    #print(marcheflag, rastate, decstate)

    if marcheflag:
      marche = c.create_oval(stopx-rx, 100-ry, stopx+rx, 100+ry, fill = "lightgreen")
    else:
      marche = c.create_oval(stopx-rx, 100-ry, stopx+rx, 100+ry, fill = "darkgreen")

    c.create_text(stopx,100-3*ry/2,fill="darkblue",font=font, text="MARCHE")
    stop = c.create_oval(stopx-rx, stopy-ry, stopx+rx, stopy+ry, fill = "darkred")

    ### create declinaison circles
    if decstate == "Verti":
      decl = c.create_oval(declx-rx, line1y-ry, declx+rx, line1y+ry, fill = "lightgreen")
      decr = c.create_oval(decrx-rx, line1y-ry, decrx+rx, line1y+ry, fill = "darkgreen")
    elif decstate == "Horizon":
      decl = c.create_oval(declx-rx, line1y-ry, declx+rx, line1y+ry, fill = "darkgreen")
      decr = c.create_oval(decrx-rx, line1y-ry, decrx+rx, line1y+ry, fill = "lightgreen")
    elif decstate == "Stop":
      decl = c.create_oval(declx-rx, line1y-ry, declx+rx, line1y+ry, fill = "darkgreen")
      decr = c.create_oval(decrx-rx, line1y-ry, decrx+rx, line1y+ry, fill = "darkgreen")
    else:
      print("invalid decstate", decstate)

    ### create RA circles
    if tracking:
      dec = c.create_oval(raestrapx-rx, line1y-ry, raestrapx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raestlenx-rx, line1y-ry, raestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(rapoursx-rx, line1y-ry, rapoursx+rx, line1y+ry, fill = "lightgreen")
      dec = c.create_oval(raouestlenx-rx, line1y-ry, raouestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestrapx-rx, line1y-ry, raouestrapx+rx, line1y+ry, fill = "darkgreen")
    elif rastate == "Stop":
      dec = c.create_oval(raestrapx-rx, line1y-ry, raestrapx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raestlenx-rx, line1y-ry, raestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(rapoursx-rx, line1y-ry, rapoursx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestlenx-rx, line1y-ry, raouestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestrapx-rx, line1y-ry, raouestrapx+rx, line1y+ry, fill = "darkgreen")
    elif rastate == "OuestLent":
      dec = c.create_oval(raestrapx-rx, line1y-ry, raestrapx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raestlenx-rx, line1y-ry, raestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(rapoursx-rx, line1y-ry, rapoursx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestlenx-rx, line1y-ry, raouestlenx+rx, line1y+ry, fill = "lightgreen")
      dec = c.create_oval(raouestrapx-rx, line1y-ry, raouestrapx+rx, line1y+ry, fill = "darkgreen")
    elif rastate == "OuestRapide":
      dec = c.create_oval(raestrapx-rx, line1y-ry, raestrapx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raestlenx-rx, line1y-ry, raestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(rapoursx-rx, line1y-ry, rapoursx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestlenx-rx, line1y-ry, raouestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestrapx-rx, line1y-ry, raouestrapx+rx, line1y+ry, fill = "lightgreen")
    elif rastate == "EstLent":
      dec = c.create_oval(raestrapx-rx, line1y-ry, raestrapx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raestlenx-rx, line1y-ry, raestlenx+rx, line1y+ry, fill = "lightgreen")
      dec = c.create_oval(rapoursx-rx, line1y-ry, rapoursx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestlenx-rx, line1y-ry, raouestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestrapx-rx, line1y-ry, raouestrapx+rx, line1y+ry, fill = "darkgreen")
    elif rastate == "EstRapide":
      dec = c.create_oval(raestrapx-rx, line1y-ry, raestrapx+rx, line1y+ry, fill = "lightgreen")
      dec = c.create_oval(raestlenx-rx, line1y-ry, raestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(rapoursx-rx, line1y-ry, rapoursx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestlenx-rx, line1y-ry, raouestlenx+rx, line1y+ry, fill = "darkgreen")
      dec = c.create_oval(raouestrapx-rx, line1y-ry, raouestrapx+rx, line1y+ry, fill = "darkgreen")
    else:
      print("invalid rastate", rastate)

    dec = c.create_oval(decmx-rx, line2y-ry, decmx+rx, line2y+ry, fill = "grey")
    ra = c.create_oval(rapoursx-rx, line2y-ry, rapoursx+rx, line2y+ry, fill = "grey")

    # rearm buttons
    #rearm = c.create_oval(1000-rx, line2y-ry, 1000+rx, line2y+ry, fill = "black")
    #rearmdec = c.create_oval(1100-rx, line2y-ry, 1100+rx, line2y+ry, fill = "black")

    # display info
    r=c.create_rectangle(50, 30, 350, 130,fill="grey")
    rs = c.create_text(200,50,fill="darkblue",font=font, text="Serveur  "+PyIndiKlo.getServer())

    # update time
    dt = datetime.datetime.now()
    rm = c.create_text(200,80,fill="darkblue",font=font, text="time " + str(dt))

    # update mouse pointer
    x, y = f.winfo_pointerx(), f.winfo_pointery();
    rm = c.create_text(200,110,fill="darkblue",font=font, text="x {:d} y {:d}".format(x,y))
    
    tm = time.time()

    radec = PyIndiKlo.getRaDec()
    if tm-tml>1 and radec:
        tml = tm
        ra, dec = radec
        # update RA-Dec coordinates
        rr = c.create_rectangle(stopx-300, 185, stopx-100, 215, fill="lightgrey")
        rra = c.create_text(stopx-200,200,fill="darkblue",font=font2, text="AD {:5.3f}".format(ra))

        rh = c.create_rectangle(stopx-300, 225, stopx-100, 255, fill="lightgrey")
        ah, _ = coords.radec2hadec(long, lat, ts.now(), coords.hr2rad(ra), coords.deg2rad(dec))
        ah = coords.rad2hr(coords.regul0(ah))
        rah = c.create_text(stopx-200,240,fill="darkblue",font=font2, text="AH {:5.3f}".format(ah))

        rr = c.create_rectangle(stopx+100, 185, stopx+300, 215,fill="lightgrey")
        rdec = c.create_text(stopx+200,200,fill="darkblue",font=font2, text="Dec {:5.3f}".format(dec))
        #c.tag_lower(r,rd)

    f.update()

    f.after(100, update_control)


########################################################################

update_control()
c.bind("<Button-1>", callbackButton)

f.mainloop()

