#!/usr/bin/python3
"""Module for real time star tracking

usage: python3 startrack.py <nbstep> RaDec|AzElev

example:
  python3 track-star.py 10 RaDec
"""

import sys
import time

from skyfield.api import Star, Topos, load

nbrun = int(sys.argv[1])
coords = sys.argv[2]

# star from name in Hipparcos catalogue
# from skyfield.data import hipparcos
# with load.open(hipparcos.URL) as f:
#     df = hipparcos.load_dataframe(f)
# star = Star.from_dataframe(df.loc[int(sys.argv[3])])

# star from data
star = Star(ra_hours=(00, 42, 44.330), dec_degrees=(41, 16, 07.50))

planets = load('de421.bsp')
earth = planets['earth']

ts = load.timescale()

loc = earth + Topos('48.89389 N', '2.38833 E', elevation_m=100)

for hr in range(0, nbrun):
    t = ts.now()

    geocentric = earth.at(t).observe(star)
    astrometric = loc.at(t).observe(star)

    if coords == "RaDec":
        # increasing precision in radec coordinates
        # ra, dec, rho = geocentric.radec(epoch=t)
        # ra, dec, rho = geocentric.apparent().radec(epoch=t)
        ra, dec, rho = astrometric.apparent().radec(epoch=t)
        print(t.tt, ra.hours, dec.degrees)
    elif coords == "AzElev":
        # azelev coordinates
        alt, az, d = astrometric.apparent().altaz()
        print(t.tt, az.degrees, alt.degrees)

    time.sleep(1.)

