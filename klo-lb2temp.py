#!/usr/bin/env python3

import os
import sys

from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u
from astropy.wcs import WCS

home = os.getenv("HOME")

def coords_to_temp(l, b, fitsname = home+'/data/astro/radio/lambda_mollweide_STOCKERT+VILLA-ELISA_1420MHz_1_256.fits'):

    hItemp = fits.open(fitsname)
    hIdata = hItemp[1].data
    hIheader = hItemp[1].header
    wcs = WCS(hIheader)
    
    #c = SkyCoord(ra, dec, unit =(u.hourangle, u.degree))
    c = SkyCoord(l, b, unit = (u.degree, u.degree), frame = 'galactic')
    #c_gal = c.transform_to('galactic')

    xpix, ypix = wcs.all_world2pix(c.l, c.b, 0)
    xpix = xpix.astype(int)
    ypix = ypix.astype(int)

    temp = hIdata[ypix, xpix]
    
    return xpix, ypix, l, b, temp


l = sys.argv[1]
b = sys.argv[2]

_,_, _, _, temp = coords_to_temp(l, b)

print(temp / 1000.)
